import {
  createOrders,
  getOrdersByStatus,
  updateItem,
  deleteCart,
  deleteCartItem,
  submitOrder,
} from "../../src/services/cart";
import {
  OrderStatusEnum,
  LicenseTypeEnum,
  createOrderItem,
  getOrderItemsByStatus,
  countUserItemsByStatus,
  getOrderItemsById,
  updateOrderItem,
  deleteCartItems,
  deleteItem,
  updateOrderItemStatus,
} from "../../src/db/order-items";
import { getBalance } from "../../src/db/transactions";
import OrdersPersistenceFoundException from "../../src/errors/orders-persistence-exception";
import ResourceNotFoundException from "../../src/errors/resource-not-found-exception";
import PaymentRequiredException from "../../src/errors/payment-required-error";
import UnauthorizedException from "../../src/errors/unauthorized-exception";

import type {
  UpsertOrdersPayload,
  OrderItem,
} from "../../src/definitions/cart";
import { getRandomString } from "../helpers";

jest.mock("../../src/db/order-items");
jest.mock("../../src/db/transactions");
const user = {
  id: "9f94c4f8-d891-4802-991b-53ecff5dc659",
  email: "userEmal@educ.gouv.fr",
  username: "user@educ.gouv.fr",
  firstname: "The",
  lastname: "Admin",
};

describe("createOrders", () => {
  const userId = getRandomString();
  const order: UpsertOrdersPayload = {
    ark: "testArk",
    licenseType: LicenseTypeEnum.INDIVIDUAL,
    licenseQty: 1,
    price: 100,
    info: {
      user: {
        id: "9f94c4f8-d891-4802-991b-53ecff5dc659",
        email: "useremail@mail.com",
      },
    },
  };

  it("should create an order successfully", async () => {
    (createOrderItem as jest.Mock).mockResolvedValue([order]);

    const result = await createOrders(userId, order, user);

    expect(createOrderItem).toHaveBeenCalledWith({
      userId,
      status: OrderStatusEnum.CART,
      ...order,
      info: { user },
    });
    expect(result).toEqual([order]);
  });

  it("should throw OrdersPersistenceFoundException on error", async () => {
    (createOrderItem as jest.Mock).mockRejectedValue(new Error("DB Error"));

    await expect(createOrders(userId, order, user)).rejects.toThrow(
      OrdersPersistenceFoundException,
    );
  });
});

describe("getOrdersByStatus", () => {
  const userId = "testUserId";
  const status = OrderStatusEnum.CART;
  const from = 0;
  const size = 10;

  it("should fetch orders and return count", async () => {
    const mockOrders = [{ id: "1", price: 100 }];
    const mockCount = 1;

    (getOrderItemsByStatus as jest.Mock).mockResolvedValue(mockOrders);
    (countUserItemsByStatus as jest.Mock).mockResolvedValue(mockCount);

    const result = await getOrdersByStatus(
      userId,
      [status],
      null,
      from,
      size,
      "createdAt",
      "asc",
    );

    expect(getOrderItemsByStatus).toHaveBeenCalledWith(
      userId,
      [status],
      null,
      from,
      size,
      "createdAt",
      "asc",
    );
    expect(countUserItemsByStatus).toHaveBeenCalledWith(userId, [status], null);
    expect(result).toEqual({ result: mockOrders, count: mockCount });
  });

  it("should return empty array and count 0 if no orders found", async () => {
    (getOrderItemsByStatus as jest.Mock).mockResolvedValue([]);
    (countUserItemsByStatus as jest.Mock).mockResolvedValue(0);

    const result = await getOrdersByStatus(
      userId,
      [status],
      null,
      from,
      size,
      "createdAt",
      "asc",
    );

    expect(result).toEqual({ result: [], count: 0 });
  });
});

describe("updateItem", () => {
  const userId = "testUserId";
  const itemId = "testItemId";
  const order: OrderItem = {
    id: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
    userId: "550e8400-e29b-41d4-a716-446655440000",
    ark: "ark:/875875/Ytreg476",
    licenseType: LicenseTypeEnum.INDIVIDUAL,
    licenseQty: 1,
    price: 123.45,
    status: OrderStatusEnum.CART,
    info: { user },
    createdAt: "2020-01-01T00:00:00.123Z",
    modifiedAt: "2020-01-02T01:00:00.123Z",
  };

  it("should update CART item successfully", async () => {
    (getOrderItemsById as jest.Mock).mockResolvedValue([
      { ...order, info: { user } },
    ]);
    (updateOrderItem as jest.Mock).mockResolvedValue({
      ...order,
      info: { user },
    });

    const result = await updateItem(userId, itemId, order);

    expect(getOrderItemsById).toHaveBeenCalledWith(itemId);
    expect(updateOrderItem).toHaveBeenCalledWith(itemId, order);
    expect(result).toEqual({ ...order, info: { user } });
  });
  // it("should throw if item status is not CART", async () => {
  //   (getOrderItemsById as jest.Mock).mockResolvedValue([
  //     { ...order, info: { user }, status: OrderStatusEnum.SUBMITTED },
  //   ]);
  //   await expect(updateItem(userId, itemId, order)).rejects.toThrow(
  //     UnauthorizedException,
  //   );
  // });

  it("should throw ResourceNotFoundException if item does not exist", async () => {
    (getOrderItemsById as jest.Mock).mockResolvedValue([]);

    await expect(updateItem(userId, itemId, order)).rejects.toThrow(
      ResourceNotFoundException,
    );
  });
});

describe("deleteCart", () => {
  const userId = "testUserId";

  it("should delete cart items successfully", async () => {
    (deleteCartItems as jest.Mock).mockResolvedValue(1);

    const result = await deleteCart(userId);

    expect(deleteCartItems).toHaveBeenCalledWith(userId);
    expect(result).toBe(1);
  });
});

describe("deleteCartItem", () => {
  const userId = "testUserId";
  const itemId = "testItemId";

  it("should delete a specific cart item successfully", async () => {
    (deleteItem as jest.Mock).mockResolvedValue(1);

    const result = await deleteCartItem(userId, itemId);

    expect(deleteItem).toHaveBeenCalledWith(userId, itemId);
    expect(result).toBe(1);
  });
});

describe("submitOrder", () => {
  const userId = "testUserId";

  it("should submit order successfully if balance is sufficient", async () => {
    const mockBalance = 200;
    const mockCartItems = [
      { id: "1", price: 100, licenseQty: 1 },
      { id: "2", price: 50, licenseQty: 1 },
    ];

    (getBalance as jest.Mock).mockResolvedValue(mockBalance);
    (updateOrderItem as jest.Mock).mockImplementation((id, updatedData) => {
      const updatedItem = { ...updatedData, status: OrderStatusEnum.SUBMITTED };
      return Promise.resolve(updatedItem);
    });
    const result = await submitOrder(userId);

    expect(getBalance).toHaveBeenCalledWith(userId);
    expect(getOrderItemsByStatus).toHaveBeenCalledWith(
      userId,
      [OrderStatusEnum.CART],
      null,
      null,
      null,
      null,
      null,
    );
    // expect(updateOrderItemStatus).toHaveBeenCalledWith(
    //   mockCartItems.map((e) => e.id),
    //   userId,
    //   OrderStatusEnum.SUBMITTED,
    // );
    // expect(result).toEqual(mockCartItems);
  });

  it("should throw PaymentRequiredException if balance is insufficient", async () => {
    const mockBalance = 50;
    const mockCartItems = [{ id: "1", price: 100 }];

    (getBalance as jest.Mock).mockResolvedValue(mockBalance);
    (getOrderItemsByStatus as jest.Mock).mockResolvedValue(mockCartItems);

    await expect(submitOrder(userId)).rejects.toThrow(PaymentRequiredException);
  });
  it("should throw PaymentRequiredException if balance is exactly zero", async () => {
    const mockBalance = 0;
    const mockCartItems = [{ id: "1", price: 100 }];

    (getBalance as jest.Mock).mockResolvedValue(mockBalance);
    (updateOrderItem as jest.Mock).mockResolvedValueOnce(mockCartItems[0]);
    await expect(submitOrder(userId)).rejects.toThrow(PaymentRequiredException);
  });

  it("should submit order if cart value is zero", async () => {
    const mockBalance = 100;
    const mockCartItems = [{ id: "1", price: 0, licenseQty: 1 }];

    (getBalance as jest.Mock).mockResolvedValue(mockBalance);
    (getOrderItemsByStatus as jest.Mock).mockResolvedValue(mockCartItems);
    (updateOrderItem as jest.Mock).mockImplementation((id, updatedData) => {
      const updatedItem = { ...updatedData, status: OrderStatusEnum.SUBMITTED };
      return Promise.resolve(updatedItem);
    });
    await submitOrder(userId);
    expect(getBalance).toHaveBeenCalledWith(userId);
    expect(getOrderItemsByStatus).toHaveBeenCalledWith(
      userId,
      [OrderStatusEnum.CART],
      null,
      null,
      null,
      null,
      null,
    );
    // expect(updateOrderItemStatus).toHaveBeenCalledWith(
    //   mockCartItems.map((e) => e.id),
    //   userId,
    //   OrderStatusEnum.SUBMITTED,
    // );
    // expect(result).toEqual(mockCartItems);
  });
});

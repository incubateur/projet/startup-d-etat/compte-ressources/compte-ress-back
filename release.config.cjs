// eslint-disable-next-line @typescript-eslint/no-var-requires
const { resolve, join } = require('path')

module.exports = {
	branches: ['main'],
	plugins: [
		[
			'@semantic-release/commit-analyzer',
			{
				preset: 'angular',
				releaseRules: [{ type: 'chore', release: 'patch' }],
			},
		],
		'@semantic-release/release-notes-generator',
		'@semantic-release/changelog',
		[
			'@semantic-release/npm',
			{
				npmPublish: false,
				pkgRoot:
					process.env.GITLAB_CI && process.env.CI_PROJECT_DIR && process.env.PROJECT_FOLDER
						? resolve(join(process.env.CI_PROJECT_DIR, process.env.PROJECT_FOLDER))
						: '.',
			},
		],
		[
			'@semantic-release/exec',
			{
				verifyReleaseCmd: 'echo ${nextRelease.version} > .VERSION', // eslint-disable-line no-template-curly-in-string
			},
		],
		[
			'@semantic-release/git',
			{
				assets: ['CHANGELOG.md', 'package.json'],
				// eslint-disable-next-line no-template-curly-in-string
				message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}',
			},
		],
	],
}

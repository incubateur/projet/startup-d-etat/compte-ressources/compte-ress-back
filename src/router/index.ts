import { Router } from "express";
import cart from "./cart";
import resources from "./resources";
import wallet from "./wallet";
import notifications from "./notifications";

/**
 * API Routes.
 */
const routes = Router();

routes.use("/cart", cart);
routes.use("/resources", resources);
routes.use("/wallet", wallet);
routes.use("/notify", notifications);

export default routes;

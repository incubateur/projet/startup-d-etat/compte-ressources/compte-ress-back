import Knex from "knex";
import { asPostgresql } from "../util";
import config from "../config";
import type { Transaction } from "../definitions/transactions";

const knexInstance = Knex(asPostgresql(config.db));

export enum TransactionTypeEnum {
  CREDIT = "credit",
  DEBIT = "debit",
}

const TABLE_NAME = "transactions";

/**
 *
 * @param obj {Omit<Transaction, "id" | "createdAt">}.
 * @returns {Promise<Transaction>}
 */
export async function addTransaction(
  obj: Omit<Transaction, "id" | "createdAt">,
): Promise<Transaction> {
  const [createdItem] = await knexInstance(TABLE_NAME)
    .insert(obj)
    .returning("*");
  return {
    ...createdItem,
    total: parseFloat(createdItem.total),
    amount: parseFloat(createdItem.amount),
  };
}

/**
 * Fetch all the transaction and sum it all to get the actual balance for a user.
 * @param userId {string} User id.
 * @returns {Promise<number>}
 */
export async function getBalance(userId: string): Promise<number> {
  const credits = await knexInstance(TABLE_NAME)
    .where({ userId, type: TransactionTypeEnum.CREDIT })
    .sum("amount as total")
    .first();

  const debits = await knexInstance(TABLE_NAME)
    .where({ userId, type: TransactionTypeEnum.DEBIT })
    .sum("amount as total")
    .first();

  const totalCredits = credits?.total || 0;
  const totalDebits = debits?.total || 0;

  return totalCredits - totalDebits;
}

/**
 * Fetch all transactions for a user.
 * @param userId {string} User id.
 * @param {number} from Pagination param.
 * @param {number} size Pagination param.
 * @param sortBy
 * @param sortOrder
 * @returns {Promise<Transaction[]>}
 */
export async function getAllTransactions(
  userId: string,
  from: number,
  size: number,
  sortBy: string | null,
  sortOrder: string | null,
): Promise<Transaction[]> {
  const query = knexInstance(TABLE_NAME)
    .where({ userId })
    .select("*")
    .offset(from)
    .limit(size);
  if (sortBy && sortOrder) query.orderBy(sortBy, sortOrder);

  const transactions = await query;
  return transactions.map((e) => ({
    ...e,
    total: parseFloat(e.total),
    amount: parseFloat(e.amount),
  }));
}

/**
 * Get order items by status.
 * @param {string} userId UserId.
 * @returns {number} Items count.
 */
export async function countUserItems(userId: string) {
  const result = await knexInstance(TABLE_NAME)
    .where({ userId })
    .count<{ count: string }>("id as count")
    .first();
  return result?.count || 0;
}

/**
 * Get latest user total.
 * @param {string} userId UserId.
 * @returns {number} Latest total.
 */
export async function latestTotalResult(userId: string | undefined) {
  const result = await knexInstance(TABLE_NAME)
    .select("total")
    .where("userId", userId)
    .orderBy("createdAt", "desc")
    .limit(1)
    .first();
  return parseFloat(result?.total) || 0;
}

import { latestTotalResult, TransactionTypeEnum } from "../db/transactions";

/**
 * Get new wallet total.
 * @param userId
 * @param transactionType
 * @param amount
 * @returns {Promise<number>}
 */
export async function getNewTotal(
  userId: string | undefined,
  transactionType: TransactionTypeEnum,
  amount: number,
) {
  let latestTotal: number = await latestTotalResult(userId);
  if (transactionType === TransactionTypeEnum.CREDIT) {
    latestTotal += amount;
  } else if (transactionType === TransactionTypeEnum.DEBIT) {
    latestTotal -= amount;
  }
  return latestTotal;
}

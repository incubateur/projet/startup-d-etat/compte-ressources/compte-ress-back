import fs from "fs";
import path from "path";
import xml2js from "xml2js";
import {
  get,
  set,
  isString,
  toLower,
  map,
  isArray,
  flattenDeep,
  has,
} from "lodash";
import type {
  ResourceFilters,
  FilterMapping,
  ValuesMapping,
  filtersEnum,
  FilterItem,
} from "../definitions/resources";
import { filterTypes } from "../definitions/resources";
import ResourceNotFoundException from "../errors/resource-not-found-exception";

// Directory containing the XML files
const STATIC_DB = path.join(process.cwd(), "fakeDb"); // Change this to your directory

const FILTER_MAPPING: FilterMapping = {
  discipline: {
    source: filterTypes.CLASSIFICATION,
    value: "domaine d'enseignement",
  },
  classe: {
    source: filterTypes.CLASSIFICATION,
    value: "niveau éducatif détaillé",
  },
  type: {
    source: filterTypes.CLASSIFICATION,
    value: "label",
  },
  acces: {
    source: filterTypes.CLASSIFICATION,
    value: "type de distribution",
  },
  public: {
    source: filterTypes.CLASSIFICATION,
    value: "public cible détaillé",
  },
  editeur: {
    source: filterTypes.CONTRIBUTE,
    value: "éditeur",
  },
  titre: {
    source: filterTypes.GENERAL,
    value: "title",
  },
  description: {
    source: filterTypes.GENERAL,
    value: "description",
  },
  keyword: {
    source: filterTypes.GENERAL,
    value: "keyword",
  },
};


const VALUES_MAPPING: ValuesMapping = {
  [filterTypes.CLASSIFICATION]: {
    filterOn: "purpose.label",
    path: "taxonPath.taxon",
    key: "entry.string",
    secondaryPath: "taxonPath",
    secondaryKey: "taxon.entry.string",
  },
  [filterTypes.CONTRIBUTE]: {
    filterOn: "role.label",
    path: "entity",
  },
  [filterTypes.GENERAL]: {
    key: "string",
  },
};

function getSafeKeyValue(result: string, key: string) {
  return get(result, key, undefined);
}

export async function extractResourceToDto(file: string) {
  try {
    const filePath = path.join(STATIC_DB, file);
    const data = await fs.promises.readFile(filePath, "utf8");

    const builder = new xml2js.Builder();

    // Parse the XML file with namespace support
    const parser = new xml2js.Parser({
      explicitArray: false,
      tagNameProcessors: [xml2js.processors.stripPrefix],
    });
    const result = await parser.parseStringPromise(data);

    return builder.buildObject(result);
  } catch (err) {
    console.error("Error processing files:", err);
    return [];
  }
}

/**
 * Function to apply filters and return matching files. If all filters matches a resources, then the resource is returned.
 * @param {ResourceFilters} filters Array of filter objects {key: 'keyName', value: 'valueToMatch'}.
 * @param {number}gt Gt price filter.
 * @param gtes
 * @param gte
 * @param {number}lt Lt price filter.
 * @param {string} dir Directory where to find xml files.
 * @returns {Promise<string[]>} - Array of filenames that match the filters.
 */
export async function search(
  filters: ResourceFilters,
  gte?: number,
  lt?: number,
  dir: string = STATIC_DB,
): Promise<string[]> {
  try {
    // Read directory and get all XML files
    const files = await fs.promises.readdir(dir);

    const PricesPath = path.join(STATIC_DB, "parsed_data-prices.json");
    const pricesdata = await fs.promises.readFile(PricesPath, "utf8");

    const dbDataJson = JSON.parse(pricesdata);

    const xmlFiles = files.filter((file) => {
      const filePath = path.join(dir, file);
      const price = dbDataJson[filePath.split("fakeDb/")[1]];
      return (
        path.extname(file) === ".xml" &&
        (gte === undefined || price >= gte) &&
        (lt === undefined || price < lt)
      );
    });

    // Initialize an array to hold matching files
    const matchingFiles: string[] = [];

    // Function to parse XML and apply filters
    const applyFilters = async (file: string) => {
      const filePath = path.join(dir, file);
      const data = await fs.promises.readFile(filePath, "utf8");

      // Parse the XML file with namespace support
      const parser = new xml2js.Parser({
        explicitArray: false,
        tagNameProcessors: [xml2js.processors.stripPrefix],
        ignoreAttrs: true,
      });
      const result = await parser.parseStringPromise(data);

      // Function to get value of item
      const findFilterValue = (
        source: object,
        key: filtersEnum,
      ): string | string[] => {
        const filter: FilterItem = FILTER_MAPPING[key];
        const items: object[] = get(source, filter.source, []);
        let useSecondaryKey: boolean = false;
        if (filter.source === filterTypes.CLASSIFICATION && items) {
          const valuesObject: object[] | undefined = items.filter((e) =>
            get(
              e,
              VALUES_MAPPING[filterTypes.CLASSIFICATION].filterOn,
              "",
            ).includes(filter.value),
          );
          const fieldValue: string[] | object = valuesObject.map((e) => {
            if (has(e, VALUES_MAPPING[filterTypes.CLASSIFICATION].path) && filter.value != 'label') {
              return get(e, VALUES_MAPPING[filterTypes.CLASSIFICATION].path);
            }
            // handle special case GAR labels 
            if (has(e, VALUES_MAPPING[filterTypes.CLASSIFICATION].path) && filter.value === 'label') {
              return {
                    "entry": {
                        "string":  get(e, 'description.string')
                    }
            }
            }
            useSecondaryKey = true;
            return get(
              e,
              VALUES_MAPPING[filterTypes.CLASSIFICATION].secondaryPath,
            );
          });
          if (isArray(fieldValue)) {
            return map(flattenDeep(fieldValue), (e) =>
              useSecondaryKey
                ? get(
                    e,
                    VALUES_MAPPING[filterTypes.CLASSIFICATION].secondaryKey,
                  )
                : get(e, VALUES_MAPPING[filterTypes.CLASSIFICATION].key),
            );
          }
          return [
            get(fieldValue, VALUES_MAPPING[filterTypes.CLASSIFICATION].key),
          ];
        }
        if (filter.source === filterTypes.CONTRIBUTE && items) {
          const valuesObject: object | undefined = items.find(
            (e) =>
              get(e, VALUES_MAPPING[filterTypes.CONTRIBUTE].filterOn, null) ===
              filter.value,
          );
          return get(
            valuesObject,
            VALUES_MAPPING[filterTypes.CONTRIBUTE].path,
            [],
          );
        }
        if (filter.source === filterTypes.GENERAL && items) {
          return get(
            items,
            `${filter.value}.${VALUES_MAPPING[filterTypes.GENERAL].key}`,
            [],
          );
        }
        return [];
      };
      // Function to check if a string has a substring, case insensitive
      const includesLower = (source: string, value: string): boolean => {
        return toLower(source).includes(toLower(value));
      };
      // Function to check if filter values has a value
      const verifyMatch = (
        value: string | string[],
        filterValue: string | string[],
      ): boolean => {
        if (isString(filterValue)) {
          if (value && isString(value))
            return includesLower(value, filterValue);
          if (value && isArray(value)) {
            // verifying if value include filter value beacause value may have more character than filter value
            // example: filter.value = 'en ligne' value = 'en ligne '
            return value && value.some((e) => includesLower(e, filterValue));
          }
        } else if (isArray(filterValue)) {
          return filterValue.some((e) => {
            if (isString(value)) return includesLower(value, e);
            if (isArray(value)) return value.some((v) => includesLower(v, e));
            return false;
          });
        }
        return false;
      };
      // Check if all filters match
      const isMatch = filters.every((filter) => {
        const value = findFilterValue(result, filter.key);
        return verifyMatch(value, filter.value);
      });

      // If all filters match, add the file to the matching files list
      if (isMatch) {
        matchingFiles.push(file);
      }
    };

    // Process each XML file
    await Promise.all(xmlFiles.map(applyFilters));

    return matchingFiles;
  } catch (err) {
    console.error("Error processing files:", err);
    return [];
  }
}

/**
 * Get the file xml associated with the user id found.
 * @param {string} id RNE id (ark).
 * @returns {string|null} File path of the associated id that contains the RNE.
 */
export async function getById(id: string): Promise<string | null> {
  const filePath = path.join(STATIC_DB, "data-mappings.json");
  const data = await fs.promises.readFile(filePath, "utf8");

  const dataJson = JSON.parse(data);

  // Find the associated file with the id we are looking for
  if (Object.prototype.hasOwnProperty.call(dataJson, id)) {
    return path.join(STATIC_DB, dataJson[id]?.path);
  }

  throw new ResourceNotFoundException(id);
}

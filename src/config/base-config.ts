import { Property, Config } from "ts-convict";
import type { SchemaObj } from "convict";
import Database from "./database-config";
import type { config } from "../@types/types";
import Keycloak from "./keycloak-config";
import Jwt from "./jwt-config";

@Config({
  file: "./development.json",

  // optional parameter. Defaults to 'strict', can also be 'warn'
  validationMethod: "strict",
  formats: {
    notNull: {
      validate: (val: string, schema: SchemaObj) => {
        if (!val) {
          throw Error(`${schema.env} must be set`);
        }
      },
    },
  },
})
export class BaseConfig implements config.MyConfig {
  // ts-convict will use the Typescript type if no format given
  @Property({
    doc: "Express Base URL",
    default: "http://localhost",
    env: "BASE_URL",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public baseUrl: string;

  // ts-convict will use the Typescript type if no format given
  @Property({
    doc: "Express Port",
    default: "8888",
    env: "PORT",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public port: string;

  // ts-convict will use the Typescript type if no format given
  @Property({
    doc: "Express Base Path",
    default: "/api",
    env: "BASE_PATH",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public basePath: string;

  @Property(Database)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public db: config.Database;

  @Property(Keycloak)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public keycloak: config.Keycloak;

  @Property({
    doc: "Express cors origin",
    default: [
      "http://localhost:5173",
      "https://compte-ressource-dev.it.lajavaness.com",
    ],
    env: "CORS_OPTIONS",
    format: Array,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public cors: string;

  @Property({
    doc: "Event System API URL",
    default: "compte-ressource-dev-eventsystemmailconnector:8887",
    env: "EVENT_SYSTEM_API_BASE_URL",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public eventSystemApiBaseUrl: string;

  @Property({
    doc: "Topics to get notified on (Comma separated list)",
    default: "order.status.update",
    env: "TOPICS_TO_CONSUME",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public topic: string;

  @Property(Jwt)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public jwt: config.Jwt;

  @Property({
    doc: "Logger level",
    default: "info",
    env: "LOG_LEVEL",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public logLevel: string;

  @Property({
    doc: "Logger JSON Enabled",
    default: false,
    format: Boolean,
    env: "LOGGER_JSON_ENABLED",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public logJsonEnabled: string;

  @Property({
    doc: "Webhook Secret Key HMAC",
    default: "ThisIsNotSoSecret",
    env: "WEBHOOK_SECRET_KEY",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public webhookSecretKey: string;
}

export default BaseConfig;

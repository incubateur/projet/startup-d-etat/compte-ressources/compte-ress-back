import type { User } from "../services/keycloak";

export {};

declare global {
  namespace Express {
    export interface Request {
      user: User;
      token: string;
    }
  }
}

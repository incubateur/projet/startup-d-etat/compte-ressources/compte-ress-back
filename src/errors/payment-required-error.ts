import HttpException from "./http-exception";

class PaymentRequiredException extends HttpException {
  constructor(cartValue: number, walletValue: number) {
    super(
      402,
      `Cart value (${cartValue}) is greater than wallet balance ${walletValue}`,
    );
  }
}

export default PaymentRequiredException;

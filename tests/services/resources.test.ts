import { describe, it, expect } from "@jest/globals";
import { search } from "../../src/services/resources";
import type { ResourceFilters } from "../../src/definitions/resources";
import { filtersEnum } from "../../src/definitions/resources";
import { server } from "../../src";

describe("Resources services --> search()", () => {
  afterAll((done) => {
    server.close(done);
  });

  it("should return files that match the given filters", async () => {
    const filters: ResourceFilters = [
      { key: filtersEnum.PUBLIC, value: ["élève"] },
    ];

    const matchingFiles = await search(filters);
    expect(matchingFiles.length).toBeGreaterThan(0);
    // expect(matchingFiles.sort()).toEqual(
    //   [
    //     "AFT_AtriumFoad.xml",
    //     "ARTLY-PRODUCTION_Academy.xml",
    //     "ARTLY-PRODUCTION_Econofides.xml",
    //     "ATELIERLANGUEFRANÇAISE_eloquence.xml",
    //     "BPI_BPIBooks.xml",
    //     "CANOPE_esidoc.xml",
    //     "CNED_RCD.xml",
    //     "EDUCLEVER_Enseigno2.xml",
    //     "EDUCLEVER_Orthodidacte.xml",
    //     "EDUCLEVER_SmartEnseigno.xml",
    //     "EUROFRANCEMEDIA_CANALMETIERS.xml",
    //     "EUROFRANCEMEDIA_PARCOURSMETIERS.xml",
    //     "IMPALA_Impala.xml",
    //     "IM_Sondido.xml",
    //     "INA_lmnd222r17sm.xml",
    //     "INA_pged6t0g86qg.xml",
    //     "Iconador_louvreEdu.xml",
    //     "Iconador_textimage.xml",
    //     "MFB_edumalin.xml",
    //     "MJAM_explorateurdesmetiers.xml",
    //     "Madmagz_l8gc3vpp.xml",
    //     "MyCow_MyCow-anglais-CAS.xml",
    //     "MyCow_MyCow-francais-CAS.xml",
    //     "NOMADEDUCATION_AideReussiteScolaire_LYCEE.xml",
    //     "Nathan_3133091147495_BRNEengC4.xml",
    //     "Nathan_3133091151478_BRNEespC4.xml",
    //     "PAR_COURS_et_PAR_THEMES_OnyXP.xml",
    //     "PLATEFORME_TACIT_UNIVERSITE_RENNES_2_TACIT.xml",
    //     "PPF_9781951862107.xml",
    //     "PPF_9781951862121.xml",
    //     "PPF_9781951862145.xml",
    //     "PPF_9781951862169.xml",
    //     "REGION-GE_RessourcesInfoMetiersFormations-College.xml",
    //     "REGION-GE_RessourcesInfoMetiersFormations-lycee.xml",
    //     "SESAMATH_Labomep.xml",
    //     "SESAMATH_cm3_2017.xml",
    //     "SESAMATH_cm3_2021.xml",
    //     "SESAMATH_cm4_2017.xml",
    //     "SESAMATH_cm4_2021.xml",
    //     "SESAMATH_cm5_2017.xml",
    //     "SESAMATH_cm5_2021.xml",
    //     "SESAMATH_cm6_2017.xml",
    //     "SESAMATH_cm6_2021.xml",
    //     "SESAMATH_cscm2_2012.xml",
    //     "SESAMATH_cslp2_2014.xml",
    //     "SESAMATH_cycle4_2016.xml",
    //     "SESAMATH_ms2_2014.xml",
    //     "SESAMATH_ms6_2013.xml",
    //     "TI_texasinstruments.xml",
    //     "UNIVERSALIS_junior-edu.xml",
    //     "UNIVERSALIS_school-edu.xml",
    //     "UNIVERSALIS_senior-edu.xml",
    //     "VERT_Vert.xml",
    //     "VITTASCIENCE_vittascience.xml",
    //     "briefme_briefeco.xml",
    //     "briefme_briefme.xml",
    //     "cision_europresse-3.xml",
    //     "eduMedia_edumediaSciencesPrimaire.xml",
    //     "eduMedia_edumediaSecondaire.xml",
    //     "fjae_inforizon.xml",
    //     "hilbrt_mathematiques_1std2a.xml",
    //     "hilbrt_mathematiques_tstd2a.xml",
    //     "hilbrt_physique_1std2a.xml",
    //   ].sort(),
    // );
  });

  it("should return multiple files if they match the filters", async () => {
    const filters: ResourceFilters = [
      { key: filtersEnum.EDITEUR, value: ["hilbRt", "nonExistentValue"] },
    ];

    const matchingFiles = await search(filters);

    expect(matchingFiles.sort()).toEqual(
      [
        "hilbrt_mathematiques_tstd2a.xml",
        "hilbrt_physique_1std2a.xml",
        "hilbrt_mathematiques_1std2a.xml",
      ].sort(),
    );
  });

  it("should return no files if none match the filters", async () => {
    const filters: ResourceFilters = [
      { key: filtersEnum.PUBLIC, value: ["nonExistentValue"] },
    ];

    const matchingFiles = await search(filters);

    expect(matchingFiles).toEqual([]);
  });
});

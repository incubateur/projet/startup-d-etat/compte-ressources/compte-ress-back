import { Knex } from "knex";

const tableName = "transactions";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tableName, (table) => {
    table
      .uuid("id_new")
      .defaultTo(knex.raw("uuid_generate_v4()"))
      .notNullable();
    table.string("userId_new").notNullable();
  });

  await knex.schema.alterTable(tableName, (table) => {
    table.dropColumn("id");
    table.dropColumn("userId");
  });

  await knex.schema.alterTable(tableName, (table) => {
    table.renameColumn("id_new", "id");
    table.renameColumn("userId_new", "userId");

    // Add primary key constraint
    table.primary(["id"]);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tableName, (table) => {
    table.increments("id_old").primary();
    table.integer("userId_old").notNullable();
  });

  await knex.schema.alterTable(tableName, (table) => {
    table.dropColumn("id");
    table.dropColumn("userId");
  });

  await knex.schema.alterTable(tableName, (table) => {
    table.renameColumn("id_old", "id");
    table.renameColumn("userId_old", "userId");
  });
}

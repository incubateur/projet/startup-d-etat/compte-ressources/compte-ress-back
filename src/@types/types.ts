export declare namespace config {
  export interface KeycloakAdmin {
    secret: string; // For admin-cli client
  }

  export interface Keycloak {
    realm: string;
    authServerUrl: string;
    resource: string;
    admin: KeycloakAdmin;
  }

  export interface Database {
    client: string;
    host: string;
    database: string;
    user: string;
    password: string;
    port: number;
    pool: {
      min: number;
      max: number;
    };
  }

  export interface Jwt {
    audience: string;
    issuer: string;
    secret: string;
  }

  export interface MyConfig {
    port: string | number;
    cors: string;
    db: Database;
    topics: string; // Comma-separated strings (i.e. "order-updated,another-topic"
    jwt: Jwt;
    logLevel: string;
    logJsonEnabled: boolean;
    eventSystemApiBaseUrl: string;
    webhookSecretKey: string;
  }
}

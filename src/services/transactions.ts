import * as db from "../db/transactions";
import { TransactionTypeEnum } from "../db/transactions";
import type {
  CreateTransactionPayload,
  Transaction,
} from "../definitions/transactions";
import PersistenceFoundException from "../errors/orders-persistence-exception";
import logger from "../logger";
import { getNewTotal } from "../util/transactions";

/**
 * Persist new transactions in DB.
 * @param {string} userId
 * @returns {Promise<number>} The balance for a user.
 */
export async function getBalance(userId: string): Promise<number> {
  try {
    const result = await db.getBalance(userId);
    return result;
  } catch (err) {
    logger.error(err);
    throw new PersistenceFoundException();
  }
}

/**
 * Fetch all transactions per user.
 * @param {string} userId
 * @param from
 * @param size
 * @param sortBy
 * @param sortOrder
 * @returns {Promise<Array<Transaction>>} All the transactions.
 */
export async function getAllTransactions(
  userId: string,
  from: number,
  size: number,
  sortBy: string,
  sortOrder: string,
) {
  try {
    const result: Transaction[] = await db.getAllTransactions(
      userId,
      from,
      size,
      sortBy,
      sortOrder,
    );
    const count = await db.countUserItems(userId);
    return { result, count };
  } catch (err) {
    logger.error(err);
    throw new PersistenceFoundException();
  }
}

/**
 * Persist new transactions in DB.
 * @param {string} userId
 * @param {CreateTransactionPayload} transaction Transaction to persist.
 */
export async function createTransaction(
  userId: string,
  transaction: CreateTransactionPayload,
) {
  try {
    const newTotal: number = await getNewTotal(
      userId,
      transaction.type,
      transaction.amount,
    );
    const result = await db.addTransaction({
      userId,
      type: transaction.type,
      amount: transaction.amount,
      cause: transaction.cause,
      total: newTotal,
    });
    return result;
  } catch (err) {
    logger.error(err);
    throw new PersistenceFoundException();
  }
}

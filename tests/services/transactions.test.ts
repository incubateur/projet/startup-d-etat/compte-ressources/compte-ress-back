// __tests__/createTransaction.test.ts

import { describe, it, expect } from "@jest/globals";

import {
  createTransaction,
  getBalance,
  getAllTransactions,
} from "../../src/services/transactions";
import { TransactionTypeEnum } from "../../src/db/transactions";
import { server } from "../../src";
import { getRandomString } from "../helpers";

const from = 0;
const size = 1000;
const sortBy = "createdAt";
const sortOrder = "desc";

describe("Transaction Service", () => {
  const transaction = {
    type: TransactionTypeEnum.CREDIT,
    amount: 100,
    cause: "Initial deposit",
  };
  const transactionDebit = {
    type: TransactionTypeEnum.DEBIT,
    amount: 50,
    cause: "one purchase",
  };

  afterAll((done) => {
    server.close(done);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("createTransaction", () => {
    it("should persist a new transaction in the DB", async () => {
      const userId = getRandomString();
      const result = await createTransaction(userId, transaction);
      expect(result.cause).toEqual(transaction.cause);
      expect(result.amount).toEqual(transaction.amount);
      expect(result.type).toEqual(transaction.type);
    });
  });

  describe("getBalance", () => {
    it("should handle case where there are no transactions", async () => {
      const userId = getRandomString();

      const balance = await getBalance(userId);

      expect(balance).toBe(0);
    });
    it("should return the correct balance for the user after credit", async () => {
      const userId = getRandomString();

      let balance = await getBalance(userId);

      expect(balance).toBe(0);

      await createTransaction(userId, transaction);
      balance = await getBalance(userId);
      expect(balance).toBe(transaction.amount);
      await createTransaction(userId, transaction);
      balance = await getBalance(userId);
      expect(balance).toBe(2 * transaction.amount);
    });
    it("should return the correct balance for the user after debit", async () => {
      const userId = getRandomString();

      let balance = await getBalance(userId);

      expect(balance).toBe(0);

      await createTransaction(userId, transaction);
      await createTransaction(userId, transaction);
      await createTransaction(userId, transactionDebit);
      balance = await getBalance(userId);
      expect(balance).toBe(2 * transaction.amount - transactionDebit.amount);
    });
  });

  describe("getAllTransactions", () => {
    it("should return all transactions for the user", async () => {
      const userId = getRandomString();

      await createTransaction(userId, {
        type: TransactionTypeEnum.CREDIT,
        amount: 100,
        cause: "first transaction",
      });
      await createTransaction(userId, {
        type: TransactionTypeEnum.DEBIT,
        amount: 50,
        cause: "second transaction",
      });

      const result = await getAllTransactions(
        userId,
        from,
        size,
        sortBy,
        sortOrder,
      );

      expect(result.count).toEqual("2");
    });

    it("should handle case where there are no transactions", async () => {
      const userId = getRandomString();

      const result = await getAllTransactions(
        userId,
        from,
        size,
        sortBy,
        sortOrder,
      );

      expect(result.result).toEqual([]);
      expect(result.count).toEqual("0");
    });
  });
});

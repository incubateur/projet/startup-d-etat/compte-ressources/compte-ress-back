import HttpException from "./http-exception";

class UnauthorizedException extends HttpException {
  constructor() {
    super(401, `Not authorized`);
  }
}

export default UnauthorizedException;

import { Knex } from "knex";

const tableName = "transactions";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tableName, (table) => {
    table.decimal("amount", 14, 2).alter();
    table.decimal("total", 14, 2);
  });
  await knex.raw(`
    WITH running_totals AS (
      SELECT 
        id,
        "userId",
        SUM(
          CASE 
            WHEN type = 'credit' THEN amount
            WHEN type = 'debit' THEN -amount
            ELSE 0
          END
        ) OVER (PARTITION BY "userId" ORDER BY created_at) as running_total
      FROM transactions
    )
    UPDATE transactions
    SET total = running_totals.running_total
    FROM running_totals
    WHERE transactions.id = running_totals.id;
  `);
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("transactions", (table) => {
    table.dropColumn("total");
  });
}

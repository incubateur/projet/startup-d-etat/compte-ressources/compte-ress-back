import { TypedRequestBody, TypedRequestQuery } from "zod-express-middleware";
import { NextFunction, Response } from "express";
import * as service from "../services/transactions";
import {
  createTransactionPayloadSchema,
  itemsPagination,
} from "../definitions/transactions";
import { paginate, getSortingParams } from "../util/pagination";
import { SortingParams } from "../definitions/pagination";

export const createTransaction = async (
  req: TypedRequestBody<typeof createTransactionPayloadSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    const transaction = req.body;
    const result = await service.createTransaction(userId, transaction);
    res.send(result).status(204);
  } catch (err) {
    next(err);
  }
};

export const getBalance = async (
  req: TypedRequestQuery<typeof createTransactionPayloadSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    const result = await service.getBalance(userId);
    res.send({ balance: result }).status(200);
  } catch (err) {
    next(err);
  }
};

export const getAllTransactions = async (
  req: TypedRequestQuery<typeof itemsPagination>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    const { size = "10", page = "1" } = req.query;

    const from: number = (parseInt(page) - 1) * parseInt(size);
    const { sort = SortingParams.CREATEDDESC } = req.query;
    const { sortBy, sortOrder } = getSortingParams(sort);
    const { result, count } = await service.getAllTransactions(
      userId,
      from,
      parseInt(size),
      sortBy,
      sortOrder,
    );
    res
      .send(
        await paginate(
          { size: parseInt(size), page: parseInt(page), total: count },
          result,
        ),
      )
      .status(200);
  } catch (err) {
    next(err);
  }
};

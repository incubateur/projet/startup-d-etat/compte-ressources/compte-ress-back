import { Property } from "ts-convict";
import { config } from "../@types/types";

export default class KeycloakAdmin implements config.KeycloakAdmin {
  @Property({
    doc: "Keycloak Admin Cli Secret", // Beware, we are not talking about the realm of the token from a user.
    default: "aSecret",
    env: "SECRET",
    format: "*",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public secret: string;
}

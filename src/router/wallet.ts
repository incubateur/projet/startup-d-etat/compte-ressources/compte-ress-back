import { Router } from "express";
import {
  validateRequestBody,
  validateRequestQuery,
} from "zod-express-middleware";
import { itemsPagination } from "../definitions/cart";
import {
  createTransactionPayloadSchema,
  transactionSchema,
} from "../definitions/transactions";
import { isAuthenticated, ADMIN_ROLES } from "../services/keycloak";
import {
  createTransaction,
  getAllTransactions,
  getBalance,
} from "../controllers/transactions";

const routes = Router();

/**
 * @openapi
 * tags:
 *   - name: Wallet
 *     description: Wallet management
 */

/**
 * @openapi
 * /wallet:
 *  post:
 *    summary: Add a transaction
 *    tags:
 *      - Wallet
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    requestBody:
 *         description: Transaction
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/createTransactionPayloadSchema'
 *    responses:
 *      200:
 *        description: OK
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.post(
  "/",
  isAuthenticated(),
  validateRequestBody(createTransactionPayloadSchema),
  createTransaction,
);

/**
 * @openapi
 * /wallet/balance:
 *  get:
 *    summary: Fetch the balance for a user
 *    tags:
 *      - Wallet
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Ok
 *      404:
 *        description: Not Found
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.get(
  "/balance",
  isAuthenticated(),
  // [
  // ROLE.REFERENT_NUMERIQUE
  //  ]
  getBalance,
);

/**
 * @openapi
 * /wallet:
 *  get:
 *    summary: Fetch all transactions per user
 *    tags:
 *      - Wallet
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: OK
 *        content:
 *          application/json:
 *              schema:
 *                type: "array"
 *                items:
 *                  $ref: "#/components/schemas/transactionSchema"
 *      404:
 *        description: Not Found
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.get(
  "/",
  isAuthenticated(),
  validateRequestQuery(itemsPagination),
  getAllTransactions,
);

export default routes;

export enum SortingParams {
  CREATEDASC = "createdAt",
  CREATEDDESC = "-createdAt",
  UPDATEDASC = "modifiedAt",
  UPDATEDDESC = "-modifiedAt",
}

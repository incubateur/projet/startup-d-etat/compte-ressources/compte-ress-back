import request from "supertest";
import { getToken } from "../helpers"; // Helper function to sign Keycloak-compatible JWT
import type { MockToken } from "../helpers";
import { app, server } from "../../src/index"; // Ensure this path correctly references your index.ts file
import { TransactionTypeEnum } from "../../src/db/transactions"; // Ensure the path is correct
import * as transactionController from "../../src/controllers/transactions";

jest.mock("../../src/controllers/transactions");
jest.mock("keycloak-connect", () => {
  return jest.fn().mockImplementation(() => ({
    // @ts-expect-error Its a mock
    middleware: jest.fn().mockReturnValue((req, res, next) => {
      next();
    }),
    // @ts-expect-error Its a mock
    protect: jest.fn().mockReturnValue((req, res, next) => {
      req.token = getToken({
        roles: ["chef_etablissement"],
      });
      next();
    }),
  }));
});

describe("POST /wallet", () => {
  let token: MockToken;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll((done) => {
    server.close(done);
  });

  it("should create a transaction and return 200", async () => {
    jest
      .spyOn(transactionController, "createTransaction")
      .mockImplementation(async (req, res, next) => {
        res.status(200).send({ message: "Transaction created successfully" });
      });

    const validTransaction = {
      type: TransactionTypeEnum.CREDIT,
      amount: 1000,
      cause: "Initial deposit",
    };

    const response = await request(app)
      .post("/api/v1/wallet")
      .set("Authorization", `Bearer ${token}`)
      .send(validTransaction);

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      message: "Transaction created successfully",
    });
  });

  it("should return 400 with invalid transaction payload", async () => {
    const invalidTransaction = {
      transactionId: "invalid_uuid", // Invalid UUID
      amount: -100, // Negative amount
      currency: "US", // Invalid currency code
      date: "invalid_date", // Invalid date format
    };

    const response = await request(app)
      .post("/api/v1/wallet")
      .set("Authorization", `Bearer ${token}`)
      .send(invalidTransaction);

    expect(response.status).toBe(400);
  });

  // TODO: Needs to change the way the tests are done to make sure we can mock the token etc. RIght now, only the implementation with a good token works.
  //  I think those tests here should be placed in a 'integration-tests' folder and started with a real Keycloak and a real database.
  // it("should return 401 if not authenticated", async () => {
  //   jest.doMock('keycloak-connect', () => {
  //     return jest.fn().mockImplementation(() => ({
  //       //@ts-expect-error no
  //       middleware: jest.fn().mockReturnValue((req, res, next) => {
  //         next();
  //       }),
  //       //@ts-expect-error No
  //       protect: jest.fn().mockReturnValue((req, res, next) => {
  //         next();
  //       })
  //     }));
  //   });
  //
  //   const validTransaction = {
  //     type: TransactionTypeEnum.CREDIT,
  //     amount: 1000
  //   };
  //
  //   const response = await request(app)
  //       .post("/api/v1/wallet")
  //       .send(validTransaction);
  //
  //   expect(response.status).toBe(401);
  //   expect(createTransactionSpy).not.toHaveBeenCalled();
  // });

  it("should return 500 if controller throws an error", async () => {
    jest
      .spyOn(transactionController, "createTransaction")
      .mockImplementation(async (req, res, next) => {
        res.status(500).send({ message: "Internal server error" });
      });

    const validTransaction = {
      type: TransactionTypeEnum.CREDIT,
      amount: 1000,
      cause: "Initial deposit",
    };

    const response = await request(app)
      .post("/api/v1/wallet")
      .set("Authorization", `Bearer ${token}`)
      .send(validTransaction);

    expect(response.status).toBe(500);
    expect(response.body).toEqual({ message: "Internal server error" });
  });
});

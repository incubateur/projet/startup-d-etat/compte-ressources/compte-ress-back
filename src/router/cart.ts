import { Router } from "express";
import {
  validateRequestBody,
  validateRequestParams,
  validateRequestQuery,
} from "zod-express-middleware";
import {
  createOrders,
  updateOrders,
  deleteCart,
  deleteItem,
  submitOrder,
  PurshaseOrder,
  getOrders,
  invalidateOrderOrder,
  cancelOrderOrder,
} from "../controllers/cart";
import { isAuthenticated, ADMIN_ROLES } from "../services/keycloak";
import {
  upsertPayloadSchema,
  orderItemIdSchema,
  cartItemsPagination,
  updatePayloadSchema,
} from "../definitions/cart";

const routes = Router();

/**
 * @openapi
 * tags:
 *   - name: Orders
 *     description: API endpoints for orders management
 */

/**
 * @openapi
 * /cart:
 *  get:
 *    summary: Get cart items by optional status
 *    tags:
 *      - Orders
 *    parameters:
 *      - in: query
 *        name: page
 *        schema:
 *          type: integer
 *        description: Current page number
 *      - in: query
 *        name: size
 *        schema:
 *          type: integer
 *        description: Page size
 *      - in: query
 *        name: status
 *        schema:
 *          type: string
 *        description: Items status
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: OK
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/listCartItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */

routes.get(
  "/",
  isAuthenticated([]),
  validateRequestQuery(cartItemsPagination),
  getOrders,
);

/**
 * @openapi
 * /cart:
 *  delete:
 *    summary: Delete cart
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: OK
 *        content:
 *          application/json:
 *              schema:
 *                type: "array"
 *                items:
 *                  $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.delete("/", isAuthenticated([]), deleteCart);

/**
 * @openapi
 * /cart/items:
 *  post:
 *    summary: Save cart or add items to cart
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    requestBody:
 *         description: Transaction
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/upsertPayloadSchema'
 *    responses:
 *      201:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.post(
  "/items",
  isAuthenticated(),
  validateRequestBody(upsertPayloadSchema),
  createOrders,
);

/**
 * @openapi
 * /cart/items/{itemId}:
 *  put:
 *    summary: Update item in cart
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    requestBody:
 *         description: Transaction
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/updatePayloadSchema'
 *    responses:
 *      200:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.put(
  "/items/:itemId",
  isAuthenticated(),
  validateRequestParams(orderItemIdSchema),
  validateRequestBody(updatePayloadSchema),
  updateOrders,
);

/**
 * @openapi
 * /cart/items/{itemId}/order:
 *  put:
 *    summary: Order Item, allowed to chef_etablissement, referent_numerique and responsable_affectation roles
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.put("/items/:itemId/order", isAuthenticated(ADMIN_ROLES), PurshaseOrder);
/**
 * @openapi
 * /cart/items/{itemId}/invalidate:
 *  put:
 *    summary: Invalidate Item. allowed to chef_etablissement, referent_numerique and responsable_affectation roles
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.put(
  "/items/:itemId/invalidate",
  isAuthenticated(ADMIN_ROLES),
  // [ROLE.ENSEIGNANT, ROLE.CHEF_ETABLISSEMENT]
  invalidateOrderOrder,
);

/**
 * @openapi
 * /cart/items/{itemId}/cancel:
 *  put:
 *    summary: Cancel Item. allowed to chef_etablissement, referent_numerique and responsable_affectation roles
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.put(
  "/items/:itemId/cancel",
  isAuthenticated(ADMIN_ROLES),
  cancelOrderOrder,
);

/**
 * @openapi
 * /cart/submit:
 *  put:
 *    summary: Submit cart for validation
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Created
 *        content:
 *          application/json:
 *              schema:
 *                $ref: "#/components/schemas/OrderItemsSchema"
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.put("/submit", isAuthenticated(), submitOrder);
/**
 * @openapi
 * /cart/items/{itemId}:
 *  delete:
 *    summary: Delete an item from cart
 *    tags:
 *      - Orders
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      201:
 *        description: resource deleted successfully
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.delete(
  "/items/:itemId",
  isAuthenticated(),
  validateRequestParams(orderItemIdSchema),
  deleteItem,
);
export default routes;

import { v4 as uuidv4 } from "uuid";
import jwt from "jsonwebtoken";
import axios from "axios";
import crypto from "crypto";
import config from "../config";
import logger from "../logger";

const generateHMACSignature = (body: string) => {
  return crypto
    .createHmac("sha256", config.webhookSecretKey)
    .update(body)
    .digest("hex");
};
// Function to generate a unique identifier
const genId = (): string => uuidv4();

// Function to generate a timestamp in milliseconds
const genTs = (): number => Date.now();

export async function sendEvent(topic: string, payload: object) {
  logger.info(
    `sendEvent on topic:${topic} payload: ${JSON.stringify(payload)}`,
  );
  const url = `${config.eventSystemApiBaseUrl}/notify`;
  const token = jwt.sign(payload, config.jwt.secret, {
    issuer: config.jwt.issuer,
    audience: config.jwt.audience,
  });

  const eventObject = {
    topic,
    event: {
      clientId: "compte-ressource",
      sourceApplication: "compte-ressource-back",
      sourceEnvironment: "next",
      sourceInstance: "dev-api-back",
      eventId: genId(),
      requestId: genId(),
      timestamp: genTs(),
      payload,
    },
  };
  const signature = generateHMACSignature(JSON.stringify(eventObject));

  const query = {
    method: "POST",
    maxBodyLength: Infinity,
    url: `${config.eventSystemApiBaseUrl}/notify`,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "x-webhook-signature": signature,
    },
    data: eventObject,
  };

  try {
    console.log(JSON.stringify(query.data));
    const { data } = await axios.request(query);
    // return resp?.data;
    logger.info("request sent");
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      logger.error(
        `util/event: Failed to post to @${url} with ${JSON.stringify(payload)} ${JSON.stringify(error.response?.data)}`,
      );
    } else {
      logger.error(
        `util/event: Failed to post to @${url} with ${JSON.stringify(payload)} ${JSON.stringify(error)}`,
      );
    }

    return "Error posting event";
  }
}

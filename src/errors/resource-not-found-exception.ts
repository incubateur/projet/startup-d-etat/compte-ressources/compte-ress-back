import HttpException from "./http-exception";

class ResourceNotFoundException extends HttpException {
  constructor(id: string) {
    super(404, `Resource with id ${id} not found`);
  }
}

export default ResourceNotFoundException;

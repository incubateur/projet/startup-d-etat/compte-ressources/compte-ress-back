import type { Knex } from "knex";
import { LicenseTypeEnum, OrderStatusEnum } from "../../src/db/order-items";

const tableName = "order-items";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.table(tableName, (table) => {
    table.json("info");
    table
      .enu("status_tmp", Object.values(OrderStatusEnum))
      .notNullable()
      .defaultTo(OrderStatusEnum.CART);
    table
      .enu("licenseType_tmp", Object.values(LicenseTypeEnum))
      .notNullable()
      .defaultTo(LicenseTypeEnum.INDIVIDUAL);
  });

  await knex(tableName)
    .where("licenseType", "INDIVIDUAL")
    .update({ licenseType_tmp: LicenseTypeEnum.INDIVIDUAL });

  await knex(tableName)
    .where("licenseType", "ESTABLISHMENT")
    .update({ licenseType_tmp: LicenseTypeEnum.ESTABLISHMENT });

  await knex(tableName)
    .where("status", "CART")
    .update({ status_tmp: OrderStatusEnum.CART });

  await knex(tableName)
    .where("status", "SUBMITTED")
    .update({ status_tmp: OrderStatusEnum.SUBMITTED });
  await knex(tableName)
    .where("status", "VALIDATED")
    .update({ status_tmp: OrderStatusEnum.VALIDATED });

  await knex(tableName)
    .where("status", "INVALIDATED")
    .update({ status_tmp: OrderStatusEnum.INVALIDATED });

  await knex(tableName)
    .where("status", "CANCELED")
    .update({ status_tmp: OrderStatusEnum.CANCELED });

  await knex(tableName)
    .where("status", "ORDERED")
    .update({ status_tmp: OrderStatusEnum.ORDERED });

  await knex(tableName)
    .where("status", "ASSIGNED")
    .update({ status_tmp: OrderStatusEnum.ASSIGNED });

  await knex.schema.table(tableName, (table) => {
    table.dropColumns("status");
    table.renameColumn("status_tmp", "status");
    table.dropColumns("licenseType");
    table.renameColumn("licenseType_tmp", "licenseType");
  });
}
export async function down(knex: Knex): Promise<void> {
  await knex.schema.table(tableName, (table) => {
    table
      .enu("status_tmp", [
        "CART",
        "SUBMITTED",
        "VALIDATED",
        "INVALIDATED",
        "CANCELED",
        "ORDERED",
        "ASSIGNED",
      ])
      .notNullable()
      .defaultTo("CART");
    table
      .enu("licenseType_tmp", ["INDIVIDUAL", "ESTABLISHMENT"])
      .notNullable()
      .defaultTo("INDIVIDUAL");
  });

  await knex(tableName)
    .where("licenseType", "individual")
    .update({ licenseType_tmp: "INDIVIDUAL" });

  await knex(tableName)
    .where("licenseType", "establishment")
    .update({ licenseType_tmp: "ESTABLISHMENT" });

  await knex(tableName).where("status", "cart").update({ status_tmp: "CART" });
  await knex(tableName)
    .where("status", "submitted")
    .update({ status_tmp: "SUBMITTED" });
  await knex(tableName)
    .where("status", "validated")
    .update({ status_tmp: "VALIDATED" });
  await knex(tableName)
    .where("status", "invalidated")
    .update({ status_tmp: "INVALIDATED" });
  await knex(tableName)
    .where("status", "canceled")
    .update({ status_tmp: "CANCELED" });
  await knex(tableName)
    .where("status", "ordered")
    .update({ status_tmp: "ORDERED" });
  await knex(tableName)
    .where("status", "assigned")
    .update({ status_tmp: "ASSIGNED" });

  await knex.schema.table(tableName, (table) => {
    table.dropColumns("status");
    table.renameColumn("status_tmp", "status");
    table.dropColumns("licenseType");
    table.renameColumn("licenseType_tmp", "licenseType");
    table.dropColumn("info");
  });
}

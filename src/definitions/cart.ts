import { z } from "../util/zod";
import { LicenseTypeEnum, OrderStatusEnum } from "../db/order-items";
import { SortingParams } from "./pagination";

const statusHistorySchema = z.object({
  cart: z.string().datetime().optional(),
  submitted: z.string().datetime().optional(),
  validated: z.string().datetime().optional(),
  invalidated: z.string().datetime().optional(),
  canceled: z.string().datetime().optional(),
  ordered: z.string().datetime().optional(),
  assigned: z.string().datetime().optional(),
});
export const orderItemSchema = z.object({
  id: z.string().openapi({ example: "df1f3419-de16-4b2b-bfb6-79a275298bf4" }),
  userId: z
    .string()
    .openapi({ example: "550e8400-e29b-41d4-a716-446655440000" }),
  ark: z
    .string()
    .regex(
      /ark:\/.+\/.+/gm,
      "Id must be in the format of 'ark:/<9999>/<AAAAAAA999>' ",
    )
    .openapi({ example: "ark:/875875/Ytreg476" }),
  licenseType: z
    .nativeEnum(LicenseTypeEnum)
    .openapi({ example: LicenseTypeEnum.INDIVIDUAL }),
  licenseQty: z.number().openapi({ example: 10 }),
  price: z.number().openapi({ example: 123.45 }),
  status: z
    .nativeEnum(OrderStatusEnum)
    .default(OrderStatusEnum.CART)
    .openapi({ example: OrderStatusEnum.VALIDATED }),
  info: z.object({
    title: z.string().optional(),
    user: z
      .object({
        id: z.string(),
        email: z.string(),
      })
      .optional(),
    statusHistory: statusHistorySchema.optional().openapi({
      example: {
        cart: "2023-07-18T00:00:00.000Z",
        submitted: "2023-07-19T00:00:00.000Z",
      },
    }),
    resource: z.object({ title: z.string() }).optional(),
  }),
  createdAt: z.string().date().openapi({ example: "2020-01-01T00:00:00.123Z" }),
  modifiedAt: z
    .string()
    .date()
    .openapi({ example: "2020-01-02T01:00:00.123Z" }),
});

export const listCartItemsSchema = z.object({
  data: z.array(orderItemSchema),
  meta: z.object({
    current_page: z.number(),
    total_page: z.number(),
    page_size: z.number(),
    total_count: z.number(),
    current_page_size: z.number(),
  }),
});

export const upsertPayloadSchema = orderItemSchema.omit({
  id: true,
  userId: true,
  status: true,
  createdAt: true,
  modifiedAt: true,
});

export const updatePayloadSchema = orderItemSchema
  .omit({
    id: true,
    userId: true,
    createdAt: true,
    modifiedAt: true,
    info: true,
  })
  .partial();
export const orderItemIdSchema = z.object({
  itemId: z.string().uuid(),
});

export const itemsPagination = z.object({
  page: z.string().default("1"),
  size: z.string().default("10"),
});

export const RessourceItemsPagination = z.object({
  page: z.string().default("1"),
  size: z.string().default("10"),
  gte: z.string().default("0"),
  lt: z.string().default("100"),
});
export const cartItemsPagination = z.object({
  page: z.string().default("1"),
  size: z.string().default("10"),
  licenseType: z
    .array(z.nativeEnum(LicenseTypeEnum))
    .default([LicenseTypeEnum.INDIVIDUAL]),
  status: z
    .array(z.nativeEnum(OrderStatusEnum))
    .default([OrderStatusEnum.CART]),
  sort: z.nativeEnum(SortingParams).default(SortingParams.CREATEDDESC),
});

export const user = z.object({
  id: z.string(),
  email: z.string(),
});

export type UserData = z.infer<typeof user>;
export type OrderItem = z.infer<typeof orderItemSchema>;
export type UpsertOrdersPayload = z.infer<typeof upsertPayloadSchema>;
export type UpdatetOrdersPayload = z.infer<typeof updatePayloadSchema>;

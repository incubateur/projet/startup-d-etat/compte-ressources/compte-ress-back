import { Request, Response, NextFunction } from "express";
import crypto from "crypto";
import config from "../config";

const validateHMACSignature = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { headers } = req;
  // @ts-expect-error Related to express.json middleware. We added the raw body so we can check the signature. The msg is not validated if we can't read the signature and won't process the request.
  const body = req.textBody;
  const digest = crypto
    .createHmac("sha256", config.webhookSecretKey)
    .update(body)
    .digest("hex");
  if (
    !headers["x-webhook-signature"] ||
    digest !== headers["x-webhook-signature"]
  ) {
    res.status(401).send({ message: "Could not verify the of the request." });
    return;
  }
  next();
};

export default validateHMACSignature;

import { Property } from "ts-convict";
import type { config } from "../@types/types";

export default class Database implements config.Database {
  // Unmanaged property env
  public client: string = "pg";

  @Property({
    doc: "Database host",
    default: "localhost",
    env: "DB_HOST",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public host: string;

  @Property({
    doc: "Database port",
    default: 5432,
    env: "DB_PORT",
    format: "int",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public port: string;

  @Property({
    doc: "Database username",
    default: "dev",
    env: "DB_USER",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public user: string;

  @Property({
    doc: "Database password",
    default: "pwd",
    env: "DB_PASSWORD",
    format: "*",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public password: string;

  @Property({
    doc: "Database name",
    default: "compte-ressource",
    env: "DB_DATABASE",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public database: string;

  // Unmanaged property env
  public pool = { min: 2, max: 10 };
}

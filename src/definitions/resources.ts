import { z } from "../util/zod";

export enum filtersEnum {
  DISCIPLINE = "discipline",
  CLASSE = "classe",
  TYPE = "type",
  ACCES = "acces",
  PUBLIC = "public",
  EDITEUR = "editeur",
  TITRE = "titre",
  DESCRIPTION = "description",
  KEYWORD = "keyword",
}

export enum filterTypes {
  CLASSIFICATION = "lom.classification",
  CONTRIBUTE = "lom.lifeCycle.contribute",
  GENERAL = "lom.general",
}

const filterMappingSchema = z.object({
  source: z.nativeEnum(filterTypes),
  value: z.string(),
});

const FILTER_MAPPING_SCHEMA = z.object({
  discipline: filterMappingSchema,
  classe: filterMappingSchema,
  type: filterMappingSchema,
  acces: filterMappingSchema,
  public: filterMappingSchema,
  editeur: filterMappingSchema,
  titre: filterMappingSchema,
  description: filterMappingSchema,
  keyword: filterMappingSchema,
});

const classificationSchema = z.object({
  filterOn: z.string(),
  path: z.string(),
  key: z.string(),
  secondaryPath: z.string(),
  secondaryKey: z.string(),
});

const lifeCycleContributeSchema = z.object({
  filterOn: z.string(),
  path: z.string(),
});

const GeneralFilterSchema = z.object({
  key: z.string(),
});

export const resourceFilterSchema = z.array(
  z.object({
    key: z.nativeEnum(filtersEnum),
    value: z.array(z.string().openapi({ example: "ark:/867565/f65gf64" })),
  }),
);

export const resourceGetPayloadSchema = z.object({
  id: z
    .string()
    .regex(
      /ark:\/.+\/.+/gm,
      "Id must be in the format of 'ark:/<9999>/<AAAAAAA>' ",
    )
    .openapi({}),
});
const VALUES_MAPPING_SCHEMA = z.object({
  [filterTypes.CLASSIFICATION]: classificationSchema,
  [filterTypes.CONTRIBUTE]: lifeCycleContributeSchema,
  [filterTypes.GENERAL]: GeneralFilterSchema,
});

export type FilterMapping = z.infer<typeof FILTER_MAPPING_SCHEMA>;
export type ValuesMapping = z.infer<typeof VALUES_MAPPING_SCHEMA>;
export type FilterItem = z.infer<typeof filterMappingSchema>;
export type ResourceFilters = z.infer<typeof resourceFilterSchema>;
export type ClassificationItem = z.infer<typeof classificationSchema>;

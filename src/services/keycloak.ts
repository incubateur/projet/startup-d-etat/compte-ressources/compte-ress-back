import { MemoryStore } from "express-session";
import KeycloakConnect from "keycloak-connect";
import type { GuardFn, Token } from "keycloak-connect";
import { jwtDecode } from "jwt-decode";
import * as express from "express";
import config from "../config";
import logger from "../logger";
import UnauthorizedException from "../errors/unauthorized-exception";
/**
 *
 *   - If the name contains no colons, then the name is taken as the entire
 *     name of a role within the current application, as specified via
 *     `clientId` or `resource` in the Keycloak Configuration.
 *   - If the name starts with the literal `realm:`, the subsequent portion
 *     is taken as the name of a _realm-level_ role.
 *   - Otherwise, the name is split at the colon, with the first portion being
 *     taken as the name of an arbitrary application, and the subsequent portion
 *     as the name of a role with that app.
 */
export enum ROLE {
  ENSEIGNANT = "enseignant",
  CHEF_ETABLISSEMENT = "chef_etablissement",
  REFERENT_NUMERIQUE = "referent_numerique",
  RESPONSABLE_AFFECTATION = "responsable_affectation",
}
export const ADMIN_ROLES: ROLE[] = [
  ROLE.CHEF_ETABLISSEMENT,
  ROLE.REFERENT_NUMERIQUE,
  ROLE.RESPONSABLE_AFFECTATION,
];
export interface User {
  id: string;
  email: string;
  username: string;
  firstname: string;
  lastname: string;
  roles: Array<ROLE>;
}

type UserInfo = {
  sub: string;
  aud: Array<string>;
  email: string;
  preferred_username: string;
  given_name: string;
  family_name: string;
  roles: Array<string>;
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
let _keycloak: KeycloakConnect.Keycloak;

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
let _memoryStore: MemoryStore;

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// let _token: Token;

const _initKeycloak = () => {
  if (_keycloak) {
    logger.warn("Keycloak already initialized");
    return _keycloak;
  }
  _memoryStore = new MemoryStore();
  _keycloak = new KeycloakConnect(
    {},
    {
      "confidential-port": 0,
      "ssl-required": "external",
      realm: config.keycloak.realm,
      "auth-server-url": config.keycloak.authServerUrl,
      resource: config.keycloak.resource,
      "bearer-only": true,
    },
  );
  logger.info("Keycloak service initialized");
  return _keycloak;
};

export const getMemoryStore = () => _memoryStore;

// const getToken = () => _token;

/**
 * Express middleware
 * Verify the role wanted from the role in the token and the role associated with the project.
 * @param {string|null} realmRoles Realm role.
 * @returns {express.RequestHandler}  Express middleware handler.
 * @private
 */
const _verifyPermissions =
  (realmRoles?: Array<string>): express.RequestHandler =>
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (realmRoles && realmRoles.length > 0) {
      const roleFound = realmRoles?.reduce((acc, value) => {
        // @ts-expect-error req.token might not contain the "hasRole" method.
        return acc || req.user?.roles?.includes(value);
      }, false);

      if (!roleFound) {
        res
          .json({
            code: 401,
            message: "NOT AUTHORIZED",
          })
          .status(401);
        throw new UnauthorizedException();
      }
    }

    // Pass
    next();
  };

/**
 * Retrieve the token from the request, save it to the request for further usage.
 * @returns {GuardFn} : Returns middleware function.
 * @private
 */
const _retrieveAndSaveToken = (): GuardFn => {
  // eslint-disable-next-line no-unused-vars
  return (token: Token, request: express.Request) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    request.token = token;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const userInfo = jwtDecode(token.token) as UserInfo;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    request.user = {
      id: userInfo.sub,
      email: userInfo.email,
      username: userInfo.preferred_username,
      firstname: userInfo.given_name,
      lastname: userInfo.family_name,
      roles: (userInfo.roles as ROLE[]) || [ROLE.ENSEIGNANT],
    };

    return true;
  };
};

/**
 * Generates an authentication middleware.
 * @param {Array<string>} [realmRoles] Realm roles needed to pass.
 * @returns {Array<express.RequestHandler>} The middlewares.
 */
export const isAuthenticated = (
  realmRoles: Array<string> = [],
): Array<express.RequestHandler> => {
  return [
    _keycloak.protect(_retrieveAndSaveToken()),
    _verifyPermissions(realmRoles),
  ];
};

export function getKeycloakInstance() {
  return _keycloak;
}

_initKeycloak();

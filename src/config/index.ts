import "reflect-metadata";

import { TSConvict } from "ts-convict";
import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { BaseConfig } from "./base-config";

dotenv.config();

// example loading default file defined in @Config
const myConfigLoader = new TSConvict<BaseConfig>(BaseConfig);
const config: BaseConfig = myConfigLoader.load();

export default config;

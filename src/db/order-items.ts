// db.ts

import Knex from "knex";
import { asPostgresql } from "../util";
import config from "../config";
import type { OrderItem } from "../definitions/cart";

const knexInstance = Knex(asPostgresql(config.db));

export enum OrderStatusEnum {
  CART = "cart",
  SUBMITTED = "submitted",
  VALIDATED = "validated",
  INVALIDATED = "invalidated",
  CANCELED = "canceled",
  ORDERED = "ordered",
  ASSIGNED = "assigned",
}

export enum LicenseTypeEnum {
  INDIVIDUAL = "individual",
  ESTABLISHMENT = "establishment",
}

const TABLE_NAME = "order-items";

/**
 * Create a new cart item.
 * @param {OrderItem} cartItem Item data.
 * @returns {Promise<OrderItem>} The created item data.
 */
export async function createOrderItem(
  cartItem: Omit<OrderItem, "id" | "createdAt" | "modifiedAt">,
): Promise<OrderItem> {
  const [createdItem] = await knexInstance(TABLE_NAME)
    .insert(cartItem)
    .returning("*");
  return { ...createdItem, price: parseFloat(createdItem.price) };
}

/**
 * Delete an cat.
 * @param userId
 * @returns {Promise<number>} 1 if delete is successful, 0 if not.
 */
export function deleteCartItems(userId: string): Promise<OrderItem> {
  return knexInstance(TABLE_NAME)
    .where({ status: OrderStatusEnum.CART, userId })
    .del();
}

/**
 * Delete item from cat.
 * @param {string} itemId Item id.
 * @param {string} userId User id.
 * @returns {Promise<number>} 1 if delete is successful, 0 if not.
 */
export function deleteItem(userId: string, itemId: string): Promise<OrderItem> {
  return knexInstance(TABLE_NAME)
    .where({ status: OrderStatusEnum.CART, userId, id: itemId })
    .del();
}
/**
 * Update an cart item by ID.
 * @param {string} id The ID of the cart item to update.
 * @param {Partial<OrderItem>} updates Item data to update.
 * @param user
 * @returns {Promise<OrderItem>} The created item.
 */
export async function updateOrderItem(
  id: string,
  updates: Partial<OrderItem>,
): Promise<OrderItem> {
  const [updatedItem] = await knexInstance(TABLE_NAME)
    .where({ id })
    .update({ ...updates, modifiedAt: knexInstance.fn.now() })
    .returning("*");
  return { ...updatedItem, price: parseFloat(updatedItem.price) };
}

/**
 * Update cart items  status by ID.
 * @param {string[]} ids The ID of the cart item to update.
 * @param {Partial<OrderItem>} updates Item data to update.
 * @param userId User id.
 * @param status New status.
 * @returns {OrderItem[]} The created item.
 */
export async function updateOrderItemStatus(
  ids: string[],
  userId: string | null,
  status: OrderStatusEnum,
): Promise<OrderItem[]> {
  const query = knexInstance(TABLE_NAME);
  if (userId) query.where({ userId });
  const [updatedItem] = await query
    .update({
      status,
      modifiedAt: knexInstance.fn.now(),
    })
    .whereIn("id", ids)
    .returning("*");
  return updatedItem;
}

/**
 * Delete an cart item by ID.
 * @param {string} id The ID of the cart item to delete.
 * @returns {Promise<number>}
 */
export async function deleteOrderItem(id: string): Promise<number> {
  return knexInstance(TABLE_NAME).where({ id }).del();
}

/**
 * Get order items by status.
 * @param {string} userId UserId.
 * @param {OrderStatusEnum} status The status of the order items to retrieve.
 * @param licenseType
 * @param {number} from Pagination param.
 * @param {number} size Pagination param.
 * @param sortBy
 * @param sortOrder
 * @returns {Promise<OrderItem[]>} User items.
 */
export async function getOrderItemsByStatus(
  userId: string | null,
  status: OrderStatusEnum[],
  licenseType: LicenseTypeEnum[] | null,
  from: number | null,
  size: number | null,
  sortBy: string | null,
  sortOrder: string | null,
): Promise<OrderItem[]> {
  const query = knexInstance(TABLE_NAME).whereIn("status", status).select("*");
  if (licenseType) query.whereIn("licenseType", licenseType);
  if (userId) query.andWhere({ userId });
  if (from) query.offset(from);
  if (size) query.limit(size);
  if (sortBy && sortOrder) query.orderBy(sortBy, sortOrder);
  const rs = await query;
  return rs.map((e) => ({ ...e, price: parseFloat(e.price) }));
}

/**
 * Get order items by status.
 * @param {string} userId UserId.
 * @param {OrderStatusEnum} status The status of the order items to retrieve.
 * @param licenseType
 * @returns {number} Items count.
 */
export async function countUserItemsByStatus(
  userId: string | null,
  status: OrderStatusEnum[],
  licenseType: LicenseTypeEnum[] | null,
) {
  const query = knexInstance(TABLE_NAME).whereIn("status", status);
  if (licenseType) query.whereIn("licenseType", licenseType);
  if (userId) query.andWhere({ userId });

  const result = await query.count<{ count: string }>("id as count").first();
  return result?.count || 0;
}
/**
 * Get order items by id.
 * @param {string} itemId Item id.
 * @returns {Promise<OrderItem[]>} Item data.
 */
export async function getOrderItemsById(itemId: string): Promise<OrderItem[]> {
  const rs = await knexInstance(TABLE_NAME).where({ id: itemId }).select("*");
  return rs.map((e) => ({ ...e, price: parseFloat(e.price) }));
}

import { NextFunction, Request, Response } from "express";
import HttpException from "../errors/http-exception";
import logger from "../logger";

function errorMiddleware(
  error: HttpException,
  request: Request,
  response: Response,
  next: NextFunction,
) {
  const status = error.status || 500;
  const message = error.message || "Something went wrong";
  const newVar = {
    status,
    message,
  };
  logger.error(newVar);
  response.status(status).send(newVar);
}

export default errorMiddleware;

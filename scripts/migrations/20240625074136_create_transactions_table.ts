// src/db/migrations/xxxxxx_create_transactions_table.ts
import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable("transactions", (table) => {
    table.increments("id").primary();
    table.integer("userId").notNullable();
    table.enu("type", ["credit", "debit"]).notNullable();
    table.integer("amount").notNullable();
    table.timestamp("created_at").defaultTo(knex.fn.now());
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable("transactions");
}

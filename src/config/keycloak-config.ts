import { Property } from "ts-convict";
import KeycloakAdmin from "./keycloak-admin-config";
import type { config } from "../@types/types";

export default class Keycloak implements config.Keycloak {
  @Property({
    doc: "Keycloak Realm",
    default: "education-nationale",
    env: "REALM",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public realm: string;

  @Property({
    doc: "Keycloak Auth Token url",
    default: "http://localhost:8080",
    env: "AUTH_SERVER_URL",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public authServerUrl: string;

  @Property({
    doc: "Keycloak Resource Client id",
    default: "compte-ressource-app",
    env: "RESOURCE",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public resource: string;

  @Property(KeycloakAdmin)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public admin: config.KeycloakAdmin;
}

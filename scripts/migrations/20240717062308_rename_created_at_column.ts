import { Knex } from "knex";

const tableName = "transactions";
export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tableName, (table) => {
    table.renameColumn("created_at", "createdAt");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(tableName, (table) => {
    table.renameColumn("createdAt", "created_at");
  });
}

import { Router } from "express";
import {
  validateRequestBody,
  validateRequestQuery,
} from "zod-express-middleware";
import { isAuthenticated } from "../services/keycloak";
import { getById, search } from "../controllers/resources";
import {
  resourceFilterSchema,
  resourceGetPayloadSchema,
} from "../definitions/resources";
import { RessourceItemsPagination } from "../definitions/cart";

const routes = Router();

/**
 * @openapi
 * tags:
 *   - name: Catalog
 *     description: API endpoints for resources (RNE) management
 */

/**
 * @openapi
 * /resources/search:
 *  post:
 *    summary: Fetch all resources (rne) with filters applied
 *    tags:
 *      - Catalog
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/xml
 *    requestBody:
 *         description: Filters
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ResourceFilters'
 *    responses:
 *      200:
 *        description: Processed
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.post(
  "/search",
  isAuthenticated(),
  validateRequestQuery(RessourceItemsPagination),
  validateRequestBody(resourceFilterSchema),
  search,
);

/**
 * @openapi
 * /resources:
 *  get:
 *    summary: Fetch a resource (rne) by id
 *    tags:
 *      - Catalog
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/xml
 *    parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *    responses:
 *      200:
 *        description: Ok
 *      404:
 *        description: Not Found
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.get(
  "/",
  isAuthenticated(),
  validateRequestQuery(resourceGetPayloadSchema),
  getById,
);

export default routes;

import HttpException from "./http-exception";

class PersistenceFoundException extends HttpException {
  constructor() {
    super(500, `Couldn't persist in database. See server logs for details`);
  }
}

export default PersistenceFoundException;

import jwt from "jsonwebtoken";
import crypto from "crypto";
import config from "../src/config"; // Ensure this path correctly references your config file

// Example public key (replace with your actual public key)
const publicKey = `-----BEGIN PUBLIC KEY-----
YOUR_PUBLIC_KEY_CONTENT_HERE
-----END PUBLIC KEY-----`;

export interface MockToken {
  token: string;
  hasRole: (role: string) => boolean;
}

// Define Keycloak-specific claims
interface KeycloakTokenPayload {
  roles: string[];
  realm_access?: {
    roles: string[];
  };
  resource_access?: {
    [key: string]: {
      roles: string[];
    };
  };
}
export const getRandomString = (): string =>
  crypto.randomBytes(20).toString("hex");
export const getToken = (payload: KeycloakTokenPayload): MockToken => {
  const completePayload = {
    ...payload,
    typ: "Bearer", // Explicitly set the typ claim in the payload
    iss: `${config.keycloak.authServerUrl}/realms/${config.keycloak.realm}`, // Set the issuer to the expected realmUrl
    realm_access: {
      roles: payload.roles,
    },
    resource_access: {
      [config.keycloak.resource]: {
        roles: payload.roles,
      },
    },
    // Add additional claims as needed
  };

  const token = jwt.sign(completePayload, publicKey, {
    expiresIn: "1h",
    audience: config.jwt.audience,
  });

  return {
    token,
    hasRole: (role: string) => {
      return (
        completePayload.realm_access.roles.includes(role) ||
        completePayload.resource_access[
          config.keycloak.resource
        ].roles.includes(role)
      );
    },
  };
};

import * as xml2js from "xml2js";

export interface PaginateParams {
  page?: string | number; // Current page
  size?: string | number; // Page size
  total?: number | string; // Total number of items
}

export interface PaginationData<T> {
  meta: {
    current_page: number;
    total_page: number;
    page_size: number;
    total_count: number;
    current_page_size: number;
  };

  data: T[] | string;
}
interface SortingParams {
  sortBy: string;
  sortOrder: "asc" | "desc";
}

async function parseXmlData(xmlString: string): Promise<unknown[]> {
  const parser = new xml2js.Parser({
    explicitArray: false,
    tagNameProcessors: [xml2js.processors.stripPrefix],
    ignoreAttrs: true,
  });
  try {
    const result = await parser.parseStringPromise(xmlString);

    if (result && result?.xml?.resources?.lom) {
      return Array.isArray(result?.xml?.resources?.lom)
        ? result?.xml?.resources?.lom
        : [result?.xml?.resources?.lom];
    }
    return [];
  } catch (err) {
    console.error("Error parsing XML:", err);
    return [];
  }
}
export async function paginate<T>(
  params: PaginateParams,
  data: T[] | string,
): Promise<PaginationData<T>> {
  const page = params.page !== undefined ? parseInt(params.page.toString()) : 1;
  const size =
    params.size !== undefined ? parseInt(params.size.toString()) : 10;
  const totalCount =
    params.total !== undefined ? parseInt(params.total.toString()) : 0;
  let dataArray: T[] = [];

  const totalPage = size > 0 ? Math.ceil(totalCount / size) : 1;
  if (typeof data === "string") {
    dataArray = (await parseXmlData(data)) as T[];
  } else if (Array.isArray(data)) {
    dataArray = data;
  }
  const currentPageSize = dataArray.length;
  return {
    meta: {
      current_page: page,
      total_page: totalPage,
      page_size: size,
      total_count: totalCount,
      current_page_size: currentPageSize,
    },
    data,
  };
}

export function getSortingParams(sort: string): SortingParams {
  return {
    sortBy: sort.replace(/^-/, ""),
    sortOrder: sort.startsWith("-") ? "desc" : "asc",
  };
}

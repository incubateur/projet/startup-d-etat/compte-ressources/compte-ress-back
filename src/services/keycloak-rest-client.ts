import fetch from "node-fetch";
import { stringify } from "querystring";
import config from "../config";

type KCCredentials = {
  type: string;
  value: string;
  boolean: boolean;
  temporary?: boolean;
};

export type KCUserRepresentation = {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  credentials: KCCredentials;
};

type Options = {
  path: string;
  method: string;
  token: string;
  body?: KCUserRepresentation;
};

export type KCToken = {
  access_token: string;
  refresh_token: string;
  id_token: string;
  refresh_expires_in: number;
  expires_in: number;
  token_type: string;
  scope: string;
};

const HEADERS = { "content-type": "application/json" };

/**
 * Use this method to make a REST call to keycloak.
 * @param {Options} options Set different options. See {Options}.
 * @returns {Promise<*>} Returns a response with <code>T</code>.
 */
async function _callApi<T>(options: Options): Promise<T> {
  const response = await fetch(
    `${config.keycloak.authServerUrl}/admin/realms/${config.keycloak.realm}${options.path}`,
    {
      method: options.method,
      headers: { Authorization: `Bearer ${options.token}`, ...HEADERS },
      ...(options.body && { body: JSON.stringify(options.body) }),
    },
  );
  return (await response.json()) as T;
}

/**
 * Fetch an admin cli token to be able to use methods reserved for admin on Keycloak.
 * @returns {string} Admin Cli access token.
 */
const getAdminToken = async (): Promise<string> => {
  const data = stringify({
    grant_type: "client_credentials",
    client_id: "admin-cli",
    client_secret: config.keycloak.admin.secret,
  });

  const resp = await fetch(
    `${config.keycloak.authServerUrl}/realms/${config.keycloak.realm}/protocol/openid-connect/token`,
    {
      method: "post",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: data,
    },
  );
  const json = (await resp.json()) as KCToken;
  return json.access_token;
};

/**
 * Fetch user by id.
 * @param {string} id User uuid as it appears in keycloak.
 * @returns {Promise<KCUserRepresentation>} A user.
 */
const findById = async (id: string): Promise<KCUserRepresentation> => {
  const token = await getAdminToken();
  return _callApi<KCUserRepresentation>({
    path: `/users/${id}`,
    method: "GET",
    token,
  });
};

export default {
  findById,
};

import type { Knex } from "knex";
import { LicenseTypeEnum, OrderStatusEnum } from "../../src/db/order-items";

const tableName = "order-items";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTableIfNotExists(tableName, (table) => {
    table.uuid("id").primary().defaultTo(knex.raw("uuid_generate_v4()"));
    table.string("userId").notNullable();
    table.string("ark").notNullable();
    table.check("ark ~* '^ark:/\\d+/[^/]+$'"); // Adding regex check constraint
    table.enu("status", Object.keys(OrderStatusEnum)).notNullable();
    table.enu("licenseType", Object.keys(LicenseTypeEnum)).notNullable();
    table.integer("licenseQty").notNullable();
    table.decimal("price", 14, 2);

    table.timestamp("createdAt").defaultTo(knex.fn.now());
    table.timestamp("modifiedAt").defaultTo(knex.fn.now());
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTableIfExists(tableName);
}

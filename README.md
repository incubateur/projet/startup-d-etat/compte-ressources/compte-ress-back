# compte-ressource-back

This is the backend for the application "compte-ressource" for the client : **Education Nationale**.
Its a classic Express API made with Node. 

### Run application
Make sure you run Node 20+

#### Development
Install
```shell
yarn
```
Run
```sh
yarn start:dev
```

#### Production
```sh
yarn build
yarn start
```

### Run external tools

You will need an instance of Postgresql and Keycloak. All this can be run using this command:

```sh
docker compose up
```

#### Environment variables
You can set some environment variables at your ease. For example

| Name                           | Default                | Description                                                                                                       |
|--------------------------------|------------------------|-------------------------------------------------------------------------------------------------------------------|
| BASE_URL                       | http://localhost       | Base URL for Swagger documentation                                                                                |
| BASE_PATH                      | /api                   | Base path to access routes                                                                                        |
| PORT                           | 8887                   | Server listening port                                                                                             |
| NODE_ENV                       | development            | NODE Environment to use                                                                                           |
| DB_DATABASE                    | compte-ressource       | Postgres db                                                                                                       |
| DB_HOST                        | localhost              | Postgres host                                                                                                     |
| DB_PORT                        | 5432                   | Postgres port                                                                                                     |
| DB_USER                        | men                    | Postgres user                                                                                                     |
| DB_PASSWORD                    | password               | Postgres password                                                                                                 |
| TOPICS_TO_CONSUME              | "order.status.update"        | List of topics Kafka to listen to (i.e "order.status.update,user.logged.in")                                            |
| EVENT_SYSTEM_API_SUBSCRIBE_URL | http://localhost:8888  | Event System API Base Url                                                                                         |
| AUTH_SERVER_URL                | https://localhost:8080 | Keycloak Server Host. To use in conjunction with REALM and RESOURCE_ID                                            |
| RESOURCE                       | compte-ressource-app   | Keycloak Client Id. To use in conjunction with AUTH_SERVER_URL and REALM                                          |
| REALM                          | education-nationale    | Keycloak Realm. To use in conjunction with AUTH_SERVER_URL and RESOURCE                                           |
| JWT_ISSUER                     | event-system           | Jwt issuer. Use this in conjunction of JWT_SECRET and JWT_AUDIENCE to choose a JWT authentication strategy        |
| JWT_AUDIENCE                   | event-system           | Jwt audience. Use this in conjunction of JWT_ISSUER and JWT_SECRET to choose a JWT authentication strategy        |
| JWT_SECRET                     | notSoSecret            | Jwt secret or key. Use this in conjunction of JWT_ISSUER and JWT_AUDIENCE to choose a JWT authentication strategy |
| LOG_LEVEL                      | debug                  | General log level (['error','warn','info','debug'])                                                               |
| LOGGER_JSON_ENABLE             | false                  | Set to **true**  to enable json logging output to the console.                                                    |

### API Documentation
The swagger is available at [http://localhost:8888/swagger](http://localhost:8888/swagger)
The OpenAPI Spec file is available at [http://localhost:8888/swagger.json](http://localhost:8888/swagger.json)

### Build Docker image
```sh
docker build -t NAME --build-arg NPM_TOKEN=NPM_TOKEN .
```


import { config } from "../@types/types";
import Database = config.Database;

export const asPostgresql = (cfg: Database) => {
  return {
    client: cfg.client,
    connection: {
      user: cfg.user,
      password: cfg.password,
      port: cfg.port,
      host: cfg.host,
      database: cfg.database,
    },
  };
};

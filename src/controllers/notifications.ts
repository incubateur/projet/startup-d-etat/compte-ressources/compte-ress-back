import { TypedRequestBody } from "zod-express-middleware";
import type { Response, NextFunction } from "express";
import { eventSchema } from "../definitions/notifications";
import logger from "../logger";

export async function sendNotification(
  req: TypedRequestBody<typeof eventSchema>,
  res: Response,
  next: NextFunction,
) {
  try {
    logger.info(
      `Received notification: ${JSON.stringify(req.body.event.payload)}`,
    );
    // TODO: Send notificaiton to front-end
    res.status(201).send({ message: "ok" });
  } catch (err) {
    logger.error(err);
    // TODO: Throw new exception
  }
}

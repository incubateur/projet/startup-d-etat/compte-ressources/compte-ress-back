// Update with your myConfig settings.
import 'reflect-metadata';
import config from './src/config';
import {asPostgresql} from "./src/util";

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */

const knexconfig = {
  ...asPostgresql(config.db),
  migrations: {
    directory: './scripts/migrations',
  },
  seeds: {
    directory: './scripts/seeds',
  },
}

export default knexconfig;


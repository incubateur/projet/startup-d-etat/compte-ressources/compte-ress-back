import "reflect-metadata";
import cookieParser from "cookie-parser";
import helmet from "helmet";
import Knex from "knex";

import express, { RequestHandler } from "express";
import swaggerUi from "swagger-ui-express";
import cors from "cors";
import session from "express-session";
import logger, { expressLogger } from "./logger";
import pkg from "../package.json";
import * as keycloakService from "./services/keycloak";
import routes from "./router";
import config from "./config";
import { asPostgresql } from "./util";
import errorMiddleware from "./middlewares/error-handler";
import swagger from "./swagger";
import RouteNotFoundException from "./errors/route-not-found-exception";
import { getMemoryStore } from "./services/keycloak";

export const app = express();

/**
 * @param _req
 * @param res
 * @openapi
 * /status:
 *   get:
 *     summary: Check server status
 *     tags:
 *       - Monitoring
 *     security: []
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                 version:
 *                   type: string
 *       500:
 *         $ref: "#/responses/InternalError"
 */
const statusMiddleware: RequestHandler = (_req, res) => {
  res.json({ message: "server is up", version: pkg.version });
};

export const notFoundMiddleware: RequestHandler = (_req, _res, next) => {
  next(new RouteNotFoundException());
};

process
  .on("unhandledRejection", (reason, p) => {
    console.error(reason, "Unhandled Rejection at Promise", p);
  })
  .on("uncaughtException", (err) => {
    console.error(err, "Uncaught Exception thrown");
    process.exit(1);
  });

/**
 * Middlewares.
 */
app.enable("trust proxy");
app.use(cors({ origin: config.cors }));
app.use(
  express.json({
    limit: "10mb",
    verify(req, res, buffer) {
      // @ts-expect-error Related to express.json middleware. We added the raw body so we can check the signature
      if (req.originalUrl.search("notify") !== -1) {
        // @ts-expect-error Maybe try to check if the notify route is enough
        req.textBody = buffer.toString();
      }
    },
  }),
);
app.use(express.urlencoded({ limit: "10mb", extended: true }));
app.use(cookieParser());
app.use(expressLogger);
app.get("/status", statusMiddleware);

app.use(
  session({
    secret: "some secret",
    resave: false,
    saveUninitialized: true,
    name: "ChangeThisForEachHost",
    store: getMemoryStore(),
  }),
);
app.use(keycloakService.getKeycloakInstance().middleware());

if (process.env.NODE_ENV === "production") {
  app.use(helmet());
}

app.use(
  "/swagger",
  swaggerUi.serve,
  swaggerUi.setup(swagger, { explorer: true }),
);
app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swagger);
});

app.use(`/api/v${pkg.version.split(".")[0]}/`, routes);
app.use(notFoundMiddleware);
app.use(errorMiddleware);

/**
 * Databases connection.
 */
const knex = Knex(asPostgresql(config.db));
knex
  .raw("SELECT 1+1 AS result")
  .then(() => {
    logger.info("Connection to database successful");
  })
  .catch((err) => {
    logger.error("Unable to connect to database:", err);
  });

/**
 * Server starting.
 */
const PORT = config.port;

export const server = app.listen(PORT, () => {
  logger.info(
    `Server is running in ${process.env.NODE_ENV} mode on port ${PORT}.`,
  );
});

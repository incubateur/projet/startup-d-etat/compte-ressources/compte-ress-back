import { Request, Response, NextFunction } from "express";
import { TypedRequestBody, TypedRequestQuery } from "zod-express-middleware";
import { OrderStatusEnum } from "../db/order-items";
import * as service from "../services/cart";
import {
  upsertPayloadSchema,
  orderItemSchema,
  cartItemsPagination,
} from "../definitions/cart";
import { ADMIN_ROLES } from "../services/keycloak";

import type { OrderItem } from "../definitions/cart";
import { SortingParams } from "../definitions/pagination";
import { paginate, getSortingParams } from "../util/pagination";

export const PurshaseOrder = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { itemId } = req.params;
    const result = await service.purshareOrder(itemId);
    res.send(result).status(200);
  } catch (err) {
    next(err);
  }
};

export const invalidateOrderOrder = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { itemId } = req.params;
    const message: string = `Invalidate item`;

    const result = await service.invalidateOrder(
      itemId,
      message,
      OrderStatusEnum.INVALIDATED,
    );
    res.send(result).status(200);
  } catch (err) {
    next(err);
  }
};

export const cancelOrderOrder = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { itemId } = req.params;
    const message: string = `Cancel item`;
    const result = await service.invalidateOrder(
      itemId,
      message,
      OrderStatusEnum.CANCELED,
    );
    res.send(result).status(200);
  } catch (err) {
    next(err);
  }
};

export const getOrders = async (
  req: TypedRequestQuery<typeof cartItemsPagination>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { user } = req;
    const { size = "10", page = "1" } = req.query;

    let { status } = req.query;
    const { sort = SortingParams.CREATEDDESC } = req.query;
    const { licenseType = null } = req.query;
    const { sortBy, sortOrder } = getSortingParams(sort);
    const from: number = (parseInt(page) - 1) * parseInt(size);
    let data;
    let count;
    let result;

    if (!status) {
      status = [OrderStatusEnum.CART];
      data = await service.getOrdersByStatus(
        user.id,
        status,
        licenseType,
        from,
        parseInt(size),
        sortBy,
        sortOrder,
      );
      count = data.count;
      result = data.result;
    } else if (
      user.roles.some((role) => ADMIN_ROLES.includes(role)) &&
      !status.includes(OrderStatusEnum.CART)
    ) {
      data = await service.getOrdersByStatus(
        null,
        status,
        licenseType,
        from,
        parseInt(size),
        sortBy,
        sortOrder,
      );
      count = data.count;
      result = data.result;
    } else {
      data = await service.getOrdersByStatus(
        user.id,
        status,
        licenseType,
        from,
        parseInt(size),
        sortBy,
        sortOrder,
      );
      count = data.count;
      result = data.result;
    }

    res
      .send(
        await paginate(
          { size: parseInt(size), page: parseInt(page), total: count },
          result,
        ),
      )
      .status(200);
  } catch (err) {
    next(err);
  }
};

export const submitOrder = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    const result = await service.submitOrder(userId);
    res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};
export const deleteCart = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    await service.deleteCart(userId);
    res.sendStatus(204);
  } catch (err) {
    next(err);
  }
};
export const createOrders = async (
  req: TypedRequestBody<typeof upsertPayloadSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { user } = req;
    const orders = req.body;
    const result = await service.createOrders(user.id, orders, user);
    res.send(result).status(201);
  } catch (err) {
    next(err);
  }
};

export const updateOrders = async (
  req: TypedRequestBody<typeof orderItemSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { user } = req;
    const order: OrderItem = req.body;
    const { itemId } = req.params;
    const result = await service.updateItem(user.id, itemId, order);
    res.send(result).status(200);
  } catch (err) {
    next(err);
  }
};

export const deleteItem = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  try {
    const userId = req.user.id;
    const { itemId } = req.params;
    await service.deleteCartItem(userId, itemId);
    res.sendStatus(204);
  } catch (err) {
    next(err);
  }
};

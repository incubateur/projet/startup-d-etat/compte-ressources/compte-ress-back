import {
  getOrderItemsByStatus,
  createOrderItem,
  OrderStatusEnum,
  getOrderItemsById,
  updateOrderItem,
  deleteCartItems,
  deleteItem,
  countUserItemsByStatus,
  LicenseTypeEnum,
} from "../db/order-items";
import {
  getBalance,
  addTransaction,
  TransactionTypeEnum,
} from "../db/transactions";

import type {
  UpsertOrdersPayload,
  OrderItem,
  UserData,
} from "../definitions/cart";
import OrdersPersistenceFoundException from "../errors/orders-persistence-exception";
import ResourceNotFoundException from "../errors/resource-not-found-exception";
import PaymentRequiredException from "../errors/payment-required-error";
import logger from "../logger";
import { getNewTotal } from "../util/transactions";
import { sendEvent } from "../util/event";
import config from "../config";

/**
 * Persist new orders in DB.
 * @param {string} userId User id.
 * @param {UpsertOrdersPayload} orders Orders to persist.
 * @param order
 * @param user
 * @returns {OrderItem[]} Array of order items.
 */
export async function createOrders(
  userId: string,
  order: UpsertOrdersPayload,
  user: UserData,
) {
  try {
    const result = await createOrderItem({
      userId,
      status: OrderStatusEnum.CART,
      ark: order.ark as string,
      licenseType: order.licenseType,
      licenseQty: order.licenseQty,
      info: { ...order.info, user },
      // name: order.name,
      price: order.price,
      // info: order.info,
    });

    return result;
  } catch (err) {
    logger.error(err);
    throw new OrdersPersistenceFoundException();
  }
}

/**
 * Purshase order by changing its status and ordering it from external Api.
 * @param {string} itemId Item id to purshase.
 * @returns {Promise<OrderItem[]>} Updated item.
 */
export async function purshareOrder(itemId: string) {
  const [item]: OrderItem[] = await getOrderItemsById(itemId);
  if (!item) {
    throw new ResourceNotFoundException(itemId);
  }
  const now = new Date();
  const formattedDate = now.toISOString();
  // @TODO add external API Call (not included in prototype)
  const updatedItem = await updateOrderItem(item.id, {
    ...item,
    status: OrderStatusEnum.ORDERED,
    info: {
      ...item.info,
      statusHistory: {
        ...item.info.statusHistory,
        [OrderStatusEnum.ORDERED]: formattedDate,
      },
    },
  });
  await sendEvent(config.topic, {
    rneId: item.id,
    status: updatedItem.status,
    to: updatedItem.info.user?.email,
    rneName:
      updatedItem.info.resource?.title || updatedItem.info.title || " - ",
    submitDate: updatedItem.info.statusHistory?.submitted,
  });
  return [updatedItem];
}

/**
 * Invalidate or cancel order by changing its status and credinting user wallet.
 * @param {string} itemId Item id to purshase.
 * @param {string} message Transaction message.
 * @param status
 * @returns {Promise<OrderItem[]>} Updated item.
 */
export async function invalidateOrder(
  itemId: string,
  message: string,
  status: OrderStatusEnum,
) {
  const [item]: OrderItem[] = await getOrderItemsById(itemId);
  if (!item) {
    throw new ResourceNotFoundException(itemId);
  }
  const userId = item.info.user?.id;
  const newTotal: number = await getNewTotal(
    userId,
    TransactionTypeEnum.CREDIT,
    item.licenseQty * item.price,
  );

  await addTransaction({
    userId,
    type: TransactionTypeEnum.CREDIT,
    amount: item.licenseQty * item.price,
    cause: `${message} ${item.ark}`,
    total: newTotal,
    info: {
      title: item.info?.resource?.title || item.info?.title || "",
    },
  });
  // @TODO add external API Call in case of cancelation (not included in prototype)
  const now = new Date();
  const formattedDate = now.toISOString();
  const updatedItem = await updateOrderItem(item.id, {
    ...item,
    status,
    info: {
      ...item.info,
      statusHistory: {
        ...item.info.statusHistory,
        [status]: formattedDate,
      },
    },
  });
  try {
    logger.info(
      `sending notification to ${updatedItem.info.user?.email} ${updatedItem.status} ${updatedItem.info.statusHistory?.cart}`,
    );
    await sendEvent(config.topic, {
      rneId: item.id,
      status: updatedItem.status,
      to: updatedItem.info.user?.email,
      rneName:
        updatedItem.info.resource?.title || updatedItem.info.title || " - ",
      submitDate: updatedItem.info.statusHistory?.submitted,
    });
  } catch (error) {
    logger.error(error);
  }

  return [updatedItem];
}

/**
 * Fetch all order items by status.
 * @param {string}userId User id.
 * @param {OrderStatusEnum[]} status
 * @param page
 * @param licenseType
 * @param from
 * @param size
 * @param sortBy
 * @param sortOrder
 * @returns {Promise<OrderItemp[]>}.
 */
export async function getOrdersByStatus(
  userId: string | null,
  status: OrderStatusEnum[],
  licenseType: LicenseTypeEnum[] | null,
  from: number,
  size: number,
  sortBy: string,
  sortOrder: string,
) {
  const result = await getOrderItemsByStatus(
    userId,
    status,
    licenseType,
    from,
    size,
    sortBy,
    sortOrder,
  );
  const count = await countUserItemsByStatus(userId, status, licenseType);
  return { result, count };
}

// if (Number.isFinite(from)) searchQuery.offset(from);
// if (Number.isFinite(size)) searchQuery.limit(size);
/**
 * Fetch all order items by status.
 * @param {string} userId User id.
 * @param {string} itemId Item id.
 * @param {OrderItem} order Item body.
 * @returns {Promise<OrderItem>} Updated item.
 */
export async function updateItem(
  userId: string,
  itemId: string,
  order: OrderItem,
) {
  const [item] = await getOrderItemsById(itemId);
  if (!item) throw new ResourceNotFoundException(itemId);
  // allowed for prototype in order to validate and assign item
  // if (item.status !== OrderStatusEnum.CART) throw new UnauthorizedException();
  if (order.status && order.status !== item.status) {
    const now = new Date();
    const formattedDate = now.toISOString();
    await sendEvent(config.topic, {
      rneId: item.id,
      status: order.status,
      to: item.info.user?.email,
      rneName: item.info.resource?.title || item.info.title || " - ",
      submitDate: item.info.statusHistory?.submitted,
    });
    return updateOrderItem(itemId, {
      ...order,
      info: {
        ...item.info,
        statusHistory: {
          ...item.info.statusHistory,
          [order.status]: formattedDate,
        },
      },
    });
  }

  return updateOrderItem(itemId, order);
}
/**
 * Delete an order item by ID.
 * @param userId User id.
 * @returns {Promise<number>} 1 or 0.
 */
export function deleteCart(userId: string) {
  return deleteCartItems(userId);
}

/**
 * Delete an order item by ID.
 * @param userId
 * @param itemId
 * @returns {Promise<number>}
 */
export function deleteCartItem(userId: string, itemId: string) {
  return deleteItem(userId, itemId);
}

/**
 * Submit orders.
 * @param userId Userid.
 * @returns {Promise<OrderItem[]>}.
 */
export async function submitOrder(userId: string) {
  const balance = await getBalance(userId);
  const cartItems = await getOrderItemsByStatus(
    userId,
    [OrderStatusEnum.CART],
    null,
    null,
    null,
    null,
    null,
  );
  const cartValue = cartItems.reduce(
    (total, item) => total + item.licenseQty * item.price,
    0,
  );
  if (cartValue <= balance) {
    let newTotal: number = balance;

    await Promise.all(
      cartItems.map((item) => {
        newTotal -= item.price;
        return addTransaction({
          userId,
          type: TransactionTypeEnum.DEBIT,
          amount: item.price,
          cause: item.ark,
          total: newTotal,
          info: {
            title: item.info?.resource?.title || item?.info?.title || "",
          },
        });
      }),
    );

    const now = new Date();
    const formattedDate = now.toISOString();
    return Promise.all(
      cartItems.map((e) =>
        updateOrderItem(e.id, {
          ...e,
          status: OrderStatusEnum.SUBMITTED,
          info: {
            ...e.info,
            statusHistory: {
              ...e.info?.statusHistory,
              [OrderStatusEnum.SUBMITTED]: formattedDate,
            },
          },
        }),
      ),
    );
  }

  throw new PaymentRequiredException(cartValue, balance);
}

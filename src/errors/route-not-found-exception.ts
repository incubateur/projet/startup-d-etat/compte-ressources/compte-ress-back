import HttpException from "./http-exception";

class RouteNotFoundException extends HttpException {
  constructor() {
    super(404, "Route not found");
  }
}

export default RouteNotFoundException;

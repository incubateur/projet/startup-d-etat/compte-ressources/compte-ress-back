import { z } from "../util/zod";
import { TransactionTypeEnum } from "../db/transactions";
import { SortingParams } from "./pagination";

// Schema for transaction
export const transactionSchema = z
  .object({
    id: z.string().uuid().optional(), // Optional field for id
    userId: z
      .string()
      .nullable()
      .optional()
      .openapi({ example: "550e8400-e29b-41d4-a716-446655440000" }),
    type: z
      .nativeEnum(TransactionTypeEnum)
      .openapi({ example: TransactionTypeEnum.CREDIT }), // Type must be either "credit" or "debit"
    amount: z.number().positive().openapi({ example: 50 }), // Amount must be a positive integer
    total: z.number().positive().openapi({ example: 50 }), // Amount must be a positive integer
    cause: z.string(),
    createdAt: z.date().optional(), // Optional field for created date
    info: z
      .object({
        title: z.string(),
      })
      .optional(), // Optional field for ressource title,
  })
  .strict();

// Schema for transaction
export const createTransactionPayloadSchema = transactionSchema.omit({
  id: true,
  userId: true,
  createdAt: true,
  total: true,
  info: true,
});

// Schema for transaction
export const getBalancePayloadSchema = transactionSchema.omit({
  id: true,
  userId: true,
  createdAt: true,
  total: true,
  info: true,
});
export const itemsPagination = z.object({
  page: z.string().default("1"),
  size: z.string().default("10"),
  sort: z
    .nativeEnum(SortingParams)
    .openapi({ example: SortingParams.CREATEDDESC }),
});

export type Transaction = z.infer<typeof transactionSchema>;
export type CreateTransactionPayload = z.infer<
  typeof createTransactionPayloadSchema
>;

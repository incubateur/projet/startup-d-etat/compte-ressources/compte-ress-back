## [1.20.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.20.0...v1.20.1) (2024-09-03)


### Bug Fixes

* Fix type filter ([ac763be](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/ac763be5ab40cb59b53f7a210cb88fec20d7ce62))

# [1.20.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.19.1...v1.20.0) (2024-08-08)


### Features

* Add price filter on resources search ([9e8e62f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/9e8e62f4955096620fa83f2188c25c9b73f2d4c0))

## [1.19.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.19.0...v1.19.1) (2024-08-07)


### Bug Fixes

* Fix wrong slice on resources search ([be32e1d](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/be32e1de7fff0aeff132905a846d55d7b0fa06e8))

# [1.19.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.18.1...v1.19.0) (2024-08-07)


### Features

* Add pagination on xml search ([29d4dd3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/29d4dd340ada6ea98853ad3a428598f5f38365dd))

## [1.18.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.18.0...v1.18.1) (2024-08-07)


### Bug Fixes

* Remove files not correctly formatted ([24a42b7](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/24a42b7a39a5447ec1977dedc9146a786caa1e9c))

# [1.18.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.17.1...v1.18.0) (2024-08-07)


### Bug Fixes

* Fix broken test on resources search ([d10d6ce](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/d10d6ceb880814654f5c76b3c9a31f4a6e7dc829))
* Fix failed test ([47f7f02](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/47f7f02107a1484b0858e46292299ac86e7efe6a))


### Features

* Add new elements to xml database ([828a1cc](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/828a1ccdc6f6affd3edef71d933044366dd2aad2))

## [1.17.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.17.0...v1.17.1) (2024-08-06)

# [1.17.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.16.0...v1.17.0) (2024-08-01)


### Features

* Add resource name to transcation ([f41460c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/f41460c7813529a6d8db3847214dcf05ad157cfc))

# [1.16.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.15.0...v1.16.0) (2024-08-01)


### Features

* Replace submit transaction by transaction per item ([930b773](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/930b77302e4155de697f2022e87a156c4a2aeef6))

# [1.15.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.6...v1.15.0) (2024-07-29)


### Features

* Add email notification at every status change ([231db83](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/231db83542ad21e267b73e34f361280bb346061c))

## [1.14.6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.5...v1.14.6) (2024-07-29)


### Bug Fixes

* Fix request body on axios call for connector ([925b736](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/925b7363bfc81e16df95f6b2d4f6fdc32e4a8104))

## [1.14.5](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.4...v1.14.5) (2024-07-29)

## [1.14.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.3...v1.14.4) (2024-07-29)


### Bug Fixes

* Fix failed test ([b50430f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/b50430f2cd4425a956d3969df29db7d56bd69ef5))
* Fix header signature issue on connector call ([abc9330](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/abc93309ce9d5958920284ecdf3f0cb3f87baaf5))

## [1.14.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.2...v1.14.3) (2024-07-26)


### Bug Fixes

* Replace fetch with axios on notifications ([4983a73](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/4983a73461b522c5f3ad972ebdce398c6b4ee80c))

## [1.14.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.1...v1.14.2) (2024-07-26)

## [1.14.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.14.0...v1.14.1) (2024-07-26)


### Bug Fixes

* Fix event config url ([fd31019](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/fd31019cee7b52dbce5d643c37dcab0e4545385d))

# [1.14.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.13.1...v1.14.0) (2024-07-26)


### Features

* Add call to event system mail connector ([b65b0bd](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/b65b0bdc25c76a44650585d2854cffc4d4d8c8cc))

## [1.13.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.13.0...v1.13.1) (2024-07-26)


### Reverts

* Revert "feat: Replace submit transaction by transaction per item" ([6552026](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/6552026e566084917e2b272ed860072e48af08e8))

# [1.13.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.6...v1.13.0) (2024-07-26)


### Features

* Replace submit transaction by transaction per item ([34fb390](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/34fb3902cec1ef2259ce6ca0638a788052e2ba16))

## [1.12.6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.5...v1.12.6) (2024-07-26)


### Bug Fixes

* Edit notification topic ([f05ddcd](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/f05ddcd59d676e2009bf02ed70ce060fcf90f419))

## [1.12.5](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.4...v1.12.5) (2024-07-25)


### Bug Fixes

* Fix missed key in email payload ([1cc684d](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/1cc684db4c8639c7f352214da5aa264d03aaa82d))

## [1.12.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.3...v1.12.4) (2024-07-25)


### Bug Fixes

* Fix email variables ([32bb870](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/32bb8700b4021c1427f5683178538ecbc7ecda9f))

## [1.12.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.2...v1.12.3) (2024-07-25)


### Bug Fixes

* Edit debug logs ([b960275](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/b96027518007b7c0caeae18ddab33a9d6c65c3db))

## [1.12.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.1...v1.12.2) (2024-07-25)


### Bug Fixes

* Add debug logs ([43fd52b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/43fd52b45292a6d61df0a48f77f3f152e0848aa9))

## [1.12.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.12.0...v1.12.1) (2024-07-25)


### Bug Fixes

* Fix pagination issue ([d0cc2e4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/d0cc2e4aaf36763acebf1d251baf1f11e57dfae5))

# [1.12.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.11.4...v1.12.0) (2024-07-23)


### Features

* Add mail sending on cancel order ([30f054a](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/30f054abaed69ddfd0ed32c2d3bce302e961d267))

## [1.11.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.11.3...v1.11.4) (2024-07-22)


### Bug Fixes

* Replace id by ark in transaction cause ([7f4cbde](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/7f4cbde94891b8ba402f78cdb38db34e5c9deddd))

## [1.11.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.11.2...v1.11.3) (2024-07-19)


### Bug Fixes

* Fix cart item listing issue on admin listing ([46f405b](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/46f405b9ce37300f4fd2b1f0ac312cbfee2a3db8))

## [1.11.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.11.1...v1.11.2) (2024-07-19)


### Bug Fixes

* Add default role for user ([ff0ca93](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/ff0ca934836784286a5fc2d4aea67a6df7611fda))

## [1.11.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.11.0...v1.11.1) (2024-07-18)


### Bug Fixes

* Fix minor calculation bugs and typo issues ([c2aa6c0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c2aa6c0caf11939b3b9e5976c066d63abff02bfb))

# [1.11.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.10.4...v1.11.0) (2024-07-18)


### Features

* Handle item status history and add info.ressource ([809525f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/809525f5ad64b1cd0d67526fcf439c346770559d))

## [1.10.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.10.3...v1.10.4) (2024-07-18)


### Bug Fixes

* Fix transaction cause column type ([b58d2ea](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/b58d2ea67f98ec9b1ef63c88b6c48a59862a5be6))

## [1.10.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.10.2...v1.10.3) (2024-07-18)


### Bug Fixes

* Make all update item fields optional ([931b8a1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/931b8a1dab1484e0b525bd8fb04c1d2b37c35c23))

## [1.10.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.10.1...v1.10.2) (2024-07-18)


### Bug Fixes

* Add default status value to item zod schema ([1bfe748](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/1bfe748f165e7b5aa10834d451dd272f88d14b3b))

## [1.10.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.10.0...v1.10.1) (2024-07-18)


### Bug Fixes

* Allow item status update by user ([b67c00f](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/b67c00f7b16dae2cf6a620bf22d01afec66ca3ac))

# [1.10.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.9.3...v1.10.0) (2024-07-18)


### Features

* Add licenseType filter and enhance item listing, EDUCCR-247 ([95f20f6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/95f20f61aad29e342d1ce0296a204acf991ad587))

## [1.9.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.9.2...v1.9.3) (2024-07-18)

## [1.9.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.9.1...v1.9.2) (2024-07-17)


### Bug Fixes

* Fix required status on cart item listing ([cfbe430](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/cfbe430b6b04796974b9fc712b296be547fb2f17))

## [1.9.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.9.0...v1.9.1) (2024-07-17)


### Bug Fixes

* Fix typo ([4a9dea2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/4a9dea216f49018f637f9a7fff45ddcf21fef78d))

# [1.9.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.8.2...v1.9.0) (2024-07-17)


### Features

* Add sorting on cart items and transactions ([13b831c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/13b831c37acbcdf6d76a75621db6833d6e3ec599))

## [1.8.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.8.1...v1.8.2) (2024-07-17)


### Bug Fixes

* Allow multiple status on items listing ([d14461e](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/d14461e445233c975a3e1c15cb1527ec5f8eaa4c))

## [1.8.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.8.0...v1.8.1) (2024-07-16)


### Bug Fixes

* Add total column to transaction table ([80ef567](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/80ef5675684f6f94beff28b7e39339715cc79dbe))

# [1.8.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.7.0...v1.8.0) (2024-07-15)


### Features

* Add cart handling endpoints, EDUCCR-170 ([cf9b9a4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/cf9b9a4ad07f074da0adead88231f4aa41f67417))

# [1.7.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.6.0...v1.7.0) (2024-07-11)


### Features

* Add user data to cart items, EDUCCR-241 ([c0c52a8](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c0c52a8e8cfba4638ac0c924a928f82015330a9f))

# [1.6.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.5.3...v1.6.0) (2024-07-11)


### Features

* Add submit cart endpoint and tests ([c14b626](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c14b6263d4554dae8e6fe6ac794fbe9d84ef26aa))

## [1.5.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.5.2...v1.5.3) (2024-07-09)


### Bug Fixes

* Fix transaction issues ([2fd3cdc](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/2fd3cdc1dacb207542496a265859e59101f308b9))

## [1.5.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.5.1...v1.5.2) (2024-07-08)


### Bug Fixes

* Fix cors issue on dev env ([1a5090c](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/1a5090c75eb257b90f7fe35db4c5635d1bcde332))

## [1.5.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.5.0...v1.5.1) (2024-06-27)


### Bug Fixes

* Fix permission issue on delete item ([c77eee2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c77eee2483689460e804bd522b2f3687948dc6f2))

# [1.5.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.4.0...v1.5.0) (2024-06-27)


### Features

* Add cart CRUD endpoints ([81527af](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/81527af5f03c4acc12f1ecb6b9af8e9aa570e96c))

# [1.4.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.3.1...v1.4.0) (2024-06-27)


### Features

* Added cause column into the transactions table ([cce9b2a](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/cce9b2a35c050172f659df491b9abd5126bd9d96))

## [1.3.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.3.0...v1.3.1) (2024-06-26)


### Bug Fixes

* Standardize ressource data ([7b12379](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/7b123798de44eb6e6167fc2411d457937bf8b3da))

# [1.3.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.2.3...v1.3.0) (2024-06-26)


### Bug Fixes

* Adding migration for wallet DB ([a3fed02](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/a3fed02c8df1682388a0914d8e0c4d5024d7515d))


### Features

* Added wallet get all transactions and get balance ([c5600bc](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c5600bc32436a1f19b4bd8780bc0c8429b08f22e))

## [1.2.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.2.2...v1.2.3) (2024-06-26)


### Bug Fixes

* Make sure tests are running in band ([ed57e22](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/ed57e226695baa263011c138ac983f6cf0685963))

## [1.2.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.2.1...v1.2.2) (2024-06-26)


### Bug Fixes

* Temporary remove roles from ressources endpoints ([bb8070a](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/bb8070a9977b7535fa23372395aec3d4cd936f75))

## [1.2.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.2.0...v1.2.1) (2024-06-26)


### Bug Fixes

* Edit returned search data ([6fa37f5](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/6fa37f5f78931011f0fa8bffdf89fb7bf6fa1077))

# [1.2.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.1.1...v1.2.0) (2024-06-26)


### Bug Fixes

* Wrong key in gitlab ci ([90118ea](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/90118ea92237cf59c2bbf0c781d58294cbed5ee8))


### Features

* Added notify webhook route ([c1f91a9](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/c1f91a947df340ee71d253370284dc3bb1caa0ec))
* Added notify webhook route ([be664a7](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/be664a744be06c98ce99ed0b758b4204bd682e49))

## [1.1.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.1.0...v1.1.1) (2024-06-20)

# [1.1.0](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.31...v1.1.0) (2024-06-19)


### Features

* Make search filters have multiple values, EDUCCR-173 ([9f8f008](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/9f8f008784bc8fece57f735e799829c5a80fc230))

## [1.0.31](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.30...v1.0.31) (2024-06-19)

## [1.0.30](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.29...v1.0.30) (2024-06-14)

## [1.0.29](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.28...v1.0.29) (2024-06-14)

## [1.0.28](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.27...v1.0.28) (2024-06-13)

## [1.0.27](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.26...v1.0.27) (2024-06-12)

## [1.0.26](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.25...v1.0.26) (2024-06-12)


### Bug Fixes

* Trigger build ([95e3af2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/commit/95e3af258acb131357972836ea69b3865d72f938))

## [1.0.25](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.24...v1.0.25) (2024-06-07)

## [1.0.24](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.23...v1.0.24) (2024-06-07)

## [1.0.23](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.22...v1.0.23) (2024-06-07)

## [1.0.22](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.21...v1.0.22) (2024-06-06)

## [1.0.21](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.20...v1.0.21) (2024-06-06)

## [1.0.20](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.19...v1.0.20) (2024-06-06)

## [1.0.19](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.18...v1.0.19) (2024-06-06)

## [1.0.18](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.17...v1.0.18) (2024-06-06)

## [1.0.17](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.16...v1.0.17) (2024-06-06)

## [1.0.16](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.15...v1.0.16) (2024-06-06)

## [1.0.15](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.14...v1.0.15) (2024-06-04)

## [1.0.14](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.13...v1.0.14) (2024-06-04)

## [1.0.13](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.12...v1.0.13) (2024-05-31)

## [1.0.12](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.11...v1.0.12) (2024-05-31)

## [1.0.11](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.10...v1.0.11) (2024-05-29)

## [1.0.10](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.9...v1.0.10) (2024-05-29)

## [1.0.9](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.8...v1.0.9) (2024-05-28)

## [1.0.8](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.7...v1.0.8) (2024-05-27)

## [1.0.7](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.6...v1.0.7) (2024-05-27)

## [1.0.6](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.5...v1.0.6) (2024-05-24)

## [1.0.5](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.4...v1.0.5) (2024-05-24)

## [1.0.4](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.3...v1.0.4) (2024-05-24)

## [1.0.3](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.2...v1.0.3) (2024-05-24)

## [1.0.2](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.1...v1.0.2) (2024-05-24)

## [1.0.1](https://gitlab.com/ljn/projects/education-nationale/compte-ress-back/compare/v1.0.0...v1.0.1) (2024-05-24)

# 1.0.0 (2024-05-24)

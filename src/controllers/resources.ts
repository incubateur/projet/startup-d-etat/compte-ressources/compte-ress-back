import { Response, NextFunction } from "express";
import { TypedRequestBody, TypedRequestQuery } from "zod-express-middleware";
import * as resourcesService from "../services/resources";
import {
  resourceFilterSchema,
  resourceGetPayloadSchema,
} from "../definitions/resources";
import { extractResourceToDto } from "../services/resources";
import { paginate } from "../util/pagination";

export const search = async (
  req: TypedRequestBody<typeof resourceFilterSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { size = "24", page = "1" } = req.query;
    const { gte = "0", lt = "100" } = req.query;

    const from: number = (parseInt(page) - 1) * parseInt(size);
    const result = await resourcesService.search(
      req.body,
      parseInt(gte),
      parseInt(lt),
    );

    const resources = await Promise.all(
      result.slice(from, page * size).map(extractResourceToDto),
    );
    const s = `<xml><resources>${resources}</resources></xml>`;
    res
      .status(200)
      .send(
        await paginate(
          { size: parseInt(size), page: parseInt(page), total: result.length },
          s,
        ),
      );
    // .send({ data: s, meta: { total: result.length, page, size: 24 } });
  } catch (err) {
    console.log(err, "here");
    next(err);
  }
};

/**
 * Get the file xml associated with the user id found.
 * @param req
 * @param res
 * @param next
 * @throws {ResourceNotFoundException} If the RNE is not found.
 */
export const getById = async (
  req: TypedRequestQuery<typeof resourceGetPayloadSchema>,
  res: Response,
  next: NextFunction,
) => {
  try {
    const { id } = req.query;
    const result = await resourcesService.getById(id);
    if (result) {
      res.status(200).sendFile(result);
    }
  } catch (err) {
    next(err);
  }
};
